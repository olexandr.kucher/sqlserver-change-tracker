# SQL Server Change Tracker

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=coverage)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=olexandr.kucher_sqlserver-change-tracker&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=olexandr.kucher_sqlserver-change-tracker)

[![pipeline status](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker/badges/master/pipeline.svg)](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker/-/commits/master)

Simple Java application (under the [MIT license](LICENSE)) replicates data from Microsoft SQL Server database
using [Change Tracking mechanism](https://docs.microsoft.com/en-us/sql/relational-databases/track-changes/about-change-tracking-sql-server?view=sql-server-ver15)
and loads it into one of the supported databases or data warehouses:
* [Microsoft SQL Server](https://docs.microsoft.com/en-us/sql/sql-server/?view=sql-server-ver15) (**type=SQLServer**)
* [Snowflake](https://www.snowflake.com) (**type=Snowflake**)

### SQL Server Change Tracker build/run/dev details
* Application was built using [HotSpot AdoptOpenJDK 17](https://adoptium.net/?variant=openjdk17) and requires it to be run
* Application jar file can be downloaded from the latest success pipeline [here](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker/-/pipelines) by using _Download build:archive artifact_ option
* Application publishes to the [Docker Hub](https://hub.docker.com/repository/docker/olexandrkucher/sqlserver-change-tracker) and can be run as Docker image built with [jib](https://github.com/GoogleContainerTools/jib) (see additional details in readme at Docker Hub)
* List of known issues and planned improvements available at [GitLab Issues page](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker/-/issues)

### SQL Server Change Tracker exposes:
* http://localhost:9000/swagger-ui/index.html - Swagger documentation for the REST API (Swagger UI is available on management port)
* http://localhost:9000/metrics - Prometheus metrics
  * Application provides a lot of metrics prepared by Spring Framework
  * Additionally, application provides few custom metrics:
    * _change_tracker_init_structure_timestamp_ - time of the target storage initialization (since Unix epoch in seconds, _java.time.OffsetDateTime#toEpochSecond_)
    * _change_tracker_last_sync_timestamp_ - time of the last sync of the structure and data between the tracked database and the target storage (since Unix epoch in seconds, _java.time.OffsetDateTime#toEpochSecond_)
    * _change_tracker_tracked_tables_count_ - the counter shows the number of tables tracked by **SQL Server Change Tracker** application
    * _change_tracker_init_structure_timer_ - timer measures time spent for full initialization of the target structure (with percentile histogram)
    * _change_tracker_modify_structure_timer_ - timer measures time spent for sync the structure between the tracked database, and the target storage (add/drop/alter tables and columns, with percentile histogram)
    * _change_tracker_full_sync_timer_ - timer measures time spent to sync the structure and data between the tracked database and the target storage (with percentile histogram)
    * _change_tracker_clean_table_timer_ - timer measures time spent to clean table in the target storage before re-init (with percentile histogram)
    * _change_tracker_apply_data_changes_timer_ - timer measures time spent to transfer data from the tracked database into the target storage (with percentile histogram)
  * Example of Grafana dashboard added into application repository, see [JSON model](app-docker-compose/monitoring/grafana/dashboards/sql-server-change-tracker-grafana-dashboard.json)

### SQL Server Change Tracker requires:
* Readonly connection to the tracker SQL Server database (with enabled Change Tracking)
  * Application automatically detects all tables with enabled Change Tracking and replicates structure and data into target storage
* Connection to **settings database** (uses PostgreSQL)
  * Metadata about tracked database will be stored here
* Connection to **target storage** (one of the supported storages from the list above)

### SQL Server Change Tracker configuration
###### All the properties listed below can be overridden using _external properties file_, _Spring Cloud Config server_, _environment variables_ or another ways supported by Spring
* Main application properties:
  * _server.port_ - HTTP port of the application (default: _8080_)
  * _server.shutdown_ - sets strategy for application shutdown, _immediate_ or _graceful_ (default: _graceful_)
  * _spring.application.name_ - Java Spring application name (default: _sqlserver-change-tracker_)
  * _spring.config.import_ - path to external properties file (default: _optional:file:/config/application.properties_, _optional:configserver:http://config-server:8888_ can be set to use Spring Cloud Config instead of regular property files)
  * _spring.cloud.config.import-check.enabled_ - enables/disables check of Spring Cloud Config server on application startup (default: _false_)
* Logging properties and configuration:
  * By default application uses few destination for logs:
    * Console
    * [Sentry](https://sentry.io/welcome/)
    * [ELK Stack](https://www.elastic.co/what-is/elk-stack) based on [Logstash](https://www.elastic.co/logstash)
  * Sentry logging is disabled by default and can be enabled using next property:
    * _sentry.dsn_ - link to Sentry DSN (set it empty to disable logging errors into Sentry)
  * Logging into ELK Stack is disabled by default and can be enabled using two environment variables:
    * _LOGSTASH_HOST_ - link to Logstash instance (ex: _logstash.prod.local_)
    * _LOGSTASH_PORT_ - port of Logstash instance
  * Dev Notes:
    * There are 2 logback config files in the application repository: xml and groovy, xml file is used by default
* Management properties:
  * _management.server.port_ - port of Spring management server (default: _9000_)
  * _management.endpoints.web.exposure.include_ - comma separated list of exposed management endpoints (default: _env,info,loggers,heapdump,threaddump,prometheus,scheduledtasks,health,openapi,swagger-ui_)
  * _management.endpoints.web.base-path_ - base path to the data provided by management server (default: _/_)
  * _management.endpoints.web.path-mapping.prometheus_ - path to the endpoint returns [Prometheus](https://prometheus.io/) metrics (default: _/metrics_)
  * _management.endpoint.*<endpoint-name>*.enabled_ - enables/disables actuator endpoints (by default all the exposed endpoints enabled: _env,info,loggers,heapdump,threaddump,prometheus,scheduledtasks_)
  * _management.endpoint.health.enabled_ - enabled/disables **health endpoint** (default: _true_)
  * _management.endpoint.health.show-details_ - configures how to show health details (default: _always_)
  * _management.endpoint.health.cache.time-to-live_ - sets the cache invalidation timeout for **health endpoint** (default: _PT30S_ - 30 sec)
  * _management.health.*<health-component-name>*.enabled_ - enabled/disables concrete health components for **health endpoint** (by default enabled next groups: _db, livenessState, readinessState_)
  * _management.endpoint.health.probes.enabled_ - enables/disables default Spring Boot health probes (default: _true_)
  * _management.endpoint.health.probes.add-additional-paths_ - enables/disables additional paths for Spring Boot health probes on a main port (default: _true_)
  * _management.endpoint.health.group.liveness.include_ - configures list of health components that should be used for **liveness probe** (default: _livenessState_)
  * _management.endpoint.health.group.liveness.additional-path_ - configures additional path for **liveness probe** (default: _server:/livez_)
  * _management.endpoint.health.group.readiness.include_ - configures list of health components that should be used for **readiness probe** (default: _readinessState_)
  * _management.endpoint.health.group.readiness.additional-path_ - configures additional path for **readiness probe** (default: _server:/readyz_)
  * _spring.boot.admin.client.url_ - link to [Spring Boot Admin Server](https://codecentric.github.io/spring-boot-admin/) that will be used to register this app (default: _\<blank\>_)
* SpringDoc properties:
  * _springdoc.swagger-ui.enabled_ - enables/disables Swagger UI for this app (default: _true_)
  * _springdoc.show-actuator_ - enables/disables show of Spring Boot Actuator endpoints in Swagger UI (default: _false_)
  * _springdoc.use-management-port_ - enables/disables Swagger UI on management port (default: _true_, should be used only with _management.endpoints.web.exposure.include=openapi,swagger-ui_ property)
* Tracked database (SQL Server)
  * _tracked.database.jdbc-url_ - jdbc link to the database (ex: _jdbc:sqlserver://localhost:1433;database=change_tracker_source;_)
  * _tracked.database.username_ - login to the **tracked database**
  * _tracked.database.password_ - password to the **tracked database**
* Settings database (PostgreSQL)
  * _settings.database.jdbc-url_ - jdbc link to the database (ex: _jdbc:postgresql://localhost:5432/change_tracker_settings_)
  * _settings.database.username_ - login to the **settings database**
  * _settings.database.password_ - password to the **settings database**
* Target Storage
  * _target.storage.jdbc-url_ - jdbc link to the storage (ex: _jdbc:sqlserver://localhost:1433;database=change_tracker_target;_, _jdbc:snowflake://RX00000.europe-west1.gcp.snowflakecomputing.com/?db=CHANGE_TRACKER_TARGET_)
  * _target.storage.username_ - login to the **target storage**
  * _target.storage.password_ - password to the **target database**
  * _target.storage.type_ - type of the **target storage** (one of the supported storages from the list above)
  * _target.storage.default.schemas_ - comma separated default schemas exist in **target storage**, these schemas will not be removed even re-init is triggered (ex: _dbo_ for SQL Server or _public_ for PostgreSQL)
* Replication/sync properties:
  * _sync.data.default-batch-size_ - size of batch used to transfer data from the tracked database into target storage (default: _50000_, value should not too big to avoid Out of Memory error in case huge tables exist in tracked database)
  * _sync.data.batch-sizes_ - contains sizes of batches should be used to transfer data from the specific tracked table (_sync.data.default-batch-size_ will be used for concrete table if nothing specified here, example: _sync.data.batch-sizes.TableName=150_)
  * _sync.data.max-sync-failures-count_ - max allowed number of failures, the table will be marked for full re-import of data after this value is reached  (default: _5_)
  * _sync.data.use-buffered-data-reader_ - enables/disables usage of buffered read while transferring data from the tracked database into the target storage
    * If buffered read enabled then up to 4 pages with size=_sync.data.batch-size_ will be read into the in-memory buffer using a separate thread
    * If buffered read disabled then each next page will be read from the tracked database and written into the target storage using one thread (read and write operation will be made one by one) 
  * _sync.data.drop-missed-tables-on-sync_ - enables/disables drop of tables excluded from replication (dropped or ignored by Change Tracking in tracked database) from the target storage (default: _true_)
  * _sync.data.use-parallel-data-transfer_ - enables/disables parallel transfer of data on each sync (if enabled uses few parallel threads for data transferring, otherwise uses only one single thread, default: _true_)
  * _sync.data.parallel-threads-count_ - number of threads should be used for parallel transfer of data (will be used only when _sync.data.use-parallel-data-transfer=true_, should not be less than _2_, default: _2_)
  * _scheduled.sync.cron-expression_ - cron expression describes how often sync of tracked and target storages should be executed (default: _0 0/15 * * * ?_ - every 15 minutes)

### Run SQL Server Change Tracker using `docker-compose`
[docker-compose.yml](app-docker-compose/docker-compose.yml) file was created and added to the app repository.
Built and uploaded image to Docker Hub used by default, this behaviour can be changed if required.

* `docker-compose` can be used to easily run **SQL Server Change Tracker** locally (with minimal changes), it includes:
  * **Prometheus** - to scrap metrics produced by the application
  * **Grafana** with prepared dashboard - to visualize metrics grabbed by Prometheus
  * **Spring Boot Admin** - provides user-friendly UI for **SQL Server Change Tracker's** Spring Boot Actuator endpoints
  * PostgreSQL as **settings database** - to store required application configurations
  * [application.properties](app-docker-compose/application.properties) - already mounted properties file (**tracked.storage**, **target.database** properties should be added here before run/start)
* `docker-compose` exposes next ports:
  * **8080** - [SQL Server Change Tracker](http://localhost:9000/swagger-ui/index.html)
  * **9000** - [SQL Server Change Tracker actuator](http://localhost:9000/swagger-ui/index.html)
  * **3000** - [Grafana UI](http://localhost:3000/dashboards)
  * **9001** - [Spring Boot Web](http://localhost:9001)

### SQL Server Change Tracker Links
* [Docker Hub](https://hub.docker.com/repository/docker/olexandrkucher/sqlserver-change-tracker)
* [Sentry](https://sentry.io/organizations/self-2u/projects/sqlserver-change-tracker)
* [SonarCloud](https://sonarcloud.io/project/overview?id=olexandr.kucher_sqlserver-change-tracker)
