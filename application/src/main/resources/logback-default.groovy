import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter
import io.sentry.logback.SentryAppender
import net.logstash.logback.appender.LogstashUdpSocketAppender
import net.logstash.logback.composite.loggingevent.ArgumentsJsonProvider
import net.logstash.logback.layout.LogstashLayout

import static ch.qos.logback.classic.Level.ERROR
import static ch.qos.logback.classic.Level.INFO

/* required gradle dependency: implementation('org.codehaus.groovy:groovy:3.0.7')*/

scan()
def DEFAULT_LOG_PATTERN = "[%d{yyyy-MM-dd HH:mm:ss.SSS}]  |  %-5level [%logger{0}] [%thread] - %msg%n"
statusListener(OnConsoleStatusListener)

if (Objects.nonNull(System.getenv("LOGSTASH_HOST")) && Objects.nonNull(System.getenv("LOGSTASH_PORT"))) {
    appender("logstash", LogstashUdpSocketAppender) {
        host = System.getenv("LOGSTASH_HOST")
        port = Integer.parseInt(System.getenv("LOGSTASH_PORT"))
        layout(LogstashLayout) {
            customFields = '{"application-name":"sqlserver-change-tracker"}'
            includeMdc = true
            provider(ArgumentsJsonProvider)
        }
    }
    root(INFO, ["logstash"])
}

appender("sentry", SentryAppender) {
    filter(ThresholdFilter) {
        level = ERROR
    }
}

appender("console", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "${DEFAULT_LOG_PATTERN}"
    }
}

logger("com.microsoft.sqlserver.jdbc.internals.SQLServerConnection", ERROR)
logger("com.zaxxer.hikari.HikariDataSource", ERROR)
logger("io.kol.sqlserver.changetracker", INFO)

root(INFO, ["sentry", "console"])