package io.kol.sqlserver.changetracker.controller;

import io.kol.sqlserver.changetracker.sync.ChangeTrackingSyncService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("java:S3751")
@RestController
@Tag(name = "change-tracker-sync-controller")
class ChangeTrackingSyncController {
    private final ChangeTrackingSyncService changeTrackingSyncService;

    ChangeTrackingSyncController(ChangeTrackingSyncService changeTrackingSyncService) {
        this.changeTrackingSyncService = changeTrackingSyncService;
    }

    @GetMapping("init")
    @ApiResponse(description = "Initialization completed successfully", responseCode = "200")
    @ApiResponse(description = "Unexpected error happened while initializing target storage", responseCode = "500")
    @Operation(summary = "Initializes (if not exists) or re-initializes (if already exists) schema in the target storage")
    void initStorageStructure(
            @Parameter(description = "drops all tables from tracked schemas in the target storage if true, drops only tracked tables otherwise")
            @RequestParam boolean dropAllExistingTables
    ) {
        changeTrackingSyncService.init(dropAllExistingTables);
    }

    @GetMapping("sync")
    @ApiResponse(description = "Sync of structure structure and data completed successfully", responseCode = "200")
    @ApiResponse(description = "Unexpected error happened while syncing storage structure and data", responseCode = "500")
    @Operation(summary = "Syncs schema structure and data from the tracked database into the target storage")
    void sync(
            @Parameter(description = "drops all tables that became untracked from the target storage if true, do nothing otherwise")
            @RequestParam boolean dropMissedTables
    ) {
        changeTrackingSyncService.sync(dropMissedTables);
    }
}
