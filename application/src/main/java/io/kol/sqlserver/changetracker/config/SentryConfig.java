package io.kol.sqlserver.changetracker.config;

import io.sentry.Sentry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Objects;

@Configuration
public class SentryConfig {
    @Bean
    public InitializingBean sentryInitializer(Environment env) {
        final String dsn = env.getProperty("sentry.dsn");
        return () -> {
            if (Objects.nonNull(dsn) && !dsn.isBlank()) {
                Sentry.init(options -> {
                    options.setDsn(dsn);
                    options.setAttachThreads(true);
                });
            }
        };
    }
}
