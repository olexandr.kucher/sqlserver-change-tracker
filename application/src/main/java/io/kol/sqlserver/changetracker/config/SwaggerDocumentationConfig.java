package io.kol.sqlserver.changetracker.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerDocumentationConfig {
    @Bean
    public OpenAPI openAPI() {
        final Info info = new Info()
                .title("SQL Server Change Tracker API")
                .version("v1.0.0");
        return new OpenAPI().info(info);
    }
}