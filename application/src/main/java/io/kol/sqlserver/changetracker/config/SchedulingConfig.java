package io.kol.sqlserver.changetracker.config;

import io.kol.sqlserver.changetracker.sync.ChangeTrackingSyncService;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
class SchedulingConfig {
    private final ChangeTrackingSyncService changeTrackingSyncService;

    SchedulingConfig(ChangeTrackingSyncService changeTrackingSyncService) {
        this.changeTrackingSyncService = changeTrackingSyncService;
    }

    @NewSpan
    @Scheduled(cron = "${scheduled.sync.cron-expression}")
    void syncDataFromSourceToTargetStorage() {
        changeTrackingSyncService.sync();
    }
}
