package io.kol.sqlserver.changetracker.config;

import io.kol.sqlserver.changetracker.DateTimeProvider;
import io.kol.sqlserver.changetracker.property.DatabaseProperties;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
@EnableConfigurationProperties({DatabaseProperties.class, SyncDataProperties.class})
class AppConfig {
    @Bean
    DateTimeProvider dateTimeProvider() {
        return LocalDateTime::now;
    }
}
