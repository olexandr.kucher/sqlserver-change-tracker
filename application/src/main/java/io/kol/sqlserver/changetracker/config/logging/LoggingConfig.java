package io.kol.sqlserver.changetracker.config.logging;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoggingConfig {
    @Bean
    RequestLoggingFilter requestLoggingFilter() {
        RequestLoggingFilter filter = new RequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(false);
        filter.setBeforeMessagePrefix("[START:    ");
        filter.setBeforeMessageSuffix("]");
        filter.setAfterMessagePrefix("[COMPLETE: ");
        filter.setAfterMessageSuffix("]");
        return filter;
    }
}
