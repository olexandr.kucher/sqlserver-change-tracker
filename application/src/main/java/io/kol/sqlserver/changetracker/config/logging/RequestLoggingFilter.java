package io.kol.sqlserver.changetracker.config.logging;

import org.springframework.lang.NonNull;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import javax.servlet.http.HttpServletRequest;

class RequestLoggingFilter extends CommonsRequestLoggingFilter {
    @Override
    protected boolean shouldLog(@NonNull HttpServletRequest request) {
        return logger.isInfoEnabled();
    }

    @Override
    protected void beforeRequest(@NonNull HttpServletRequest request, @NonNull String message) {
        logger.info(message);
    }

    @Override
    protected void afterRequest(@NonNull HttpServletRequest request, @NonNull String message) {
        logger.info(message);
    }
}
