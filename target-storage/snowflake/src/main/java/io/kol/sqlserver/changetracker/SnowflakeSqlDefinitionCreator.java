package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.PrimaryKey;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;
import io.kol.sqlserver.changetracker.sql.SqlDefinitionCreator;
import io.kol.sqlserver.changetracker.sql.SqlMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.kol.sqlserver.changetracker.model.PrimaryKey.columnNamesToUpdate;

class SnowflakeSqlDefinitionCreator implements SqlDefinitionCreator {
    private static final Set<String> CHARACTER_COLUMNS_WITHOUT_LENGTH = Set.of("text", "ntext", "xml");
    private static final int MAX_COLUMN_LENGTH_INT_MARKER = -1;
    private static final String NULLABLE_COLUMN_DEFINITION = "NULL";
    private static final String NON_NULLABLE_COLUMN_DEFINITION = "NOT NULL";

    private final SqlMapper sqlMapper;

    SnowflakeSqlDefinitionCreator(SqlMapper sqlMapper) {
        this.sqlMapper = sqlMapper;
    }

    @Override
    public String createSchema(String schema) {
        return String.format("CREATE SCHEMA \"%s\"", sqlMapper.remapSqlName(schema));
    }

    @Override
    public String dropSchema(String schema) {
        return String.format("DROP SCHEMA IF EXISTS \"%s\"", sqlMapper.remapSqlName(schema));
    }

    @Override
    public String createTable(Table table) {
        return String.format(
                "CREATE TABLE \"%s\".\"%s\" (%s, CONSTRAINT \"%s_%s_PK\" PRIMARY KEY (%s))",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                columnsSqlDefinition(table),
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                columnNamesList(table.getPrimaryKey().getColumns())
        );
    }

    @Override
    public String dropTable(TargetTable table) {
        return String.format(
                "DROP TABLE IF EXISTS \"%s\".\"%s\"",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName())
        );
    }

    @Override
    public String insert(Table table) {
        return String.format(
                "INSERT INTO \"%s\".\"%s\"(%s) VALUES(%s)",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                columnNamesList(table.getColumns()),
                insertParameters(table.getColumns())
        );
    }

    @Override
    public String merge(Table table) {
        return String.format(
                """
                    MERGE INTO "%s"."%s" targetTable
                    USING (SELECT %s) AS sourceData ON %s
                    WHEN MATCHED THEN UPDATE
                        SET %s
                    WHEN NOT MATCHED THEN
                        INSERT (%s) VALUES(%s);
                """,
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                mergeColumnNamesList(table.getColumns()),
                mergeJoinColumnsSet(table.getPrimaryKey().getColumns()),
                updateColumnsSet(table.getPrimaryKey(), table.getColumns()),
                columnNamesList(table.getColumns()),
                insertParameters(table.getColumns())
        );
    }

    @Override
    public String update(Table table) {
        return String.format(
                "UPDATE \"%s\".\"%s\" SET %s WHERE %s",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                updateColumnsSet(table.getPrimaryKey(), table.getColumns()),
                whereColumnsSet(table.getPrimaryKey().getColumns())
        );
    }

    @Override
    public String delete(Table table) {
        return String.format(
                "DELETE FROM \"%s\".\"%s\" WHERE %s",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                whereColumnsSet(table.getPrimaryKey().getColumns())
        );
    }

    @Override
    public String truncate(Table table) {
        return String.format(
                "TRUNCATE TABLE \"%s\".\"%s\"",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName())
        );
    }

    @Override
    public String createColumn(Table table, Column column) {
        return String.format(
                "ALTER TABLE \"%s\".\"%s\" ADD %s",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                columnSqlDefinition(column)
        );
    }

    @Override
    public List<String> alterColumn(Table table, Column sourceColumn, Column targetColumn) {
        final List<String> sqlCommands = new ArrayList<>();
        final Column remapSourceColumn = sqlMapper.remapColumn(sourceColumn);
        if (remapSourceColumn.isNullable() && !targetColumn.isNullable()) {
            sqlCommands.add(String.format(
                    "ALTER TABLE \"%s\".\"%s\" ALTER COLUMN \"%s\" DROP NOT NULL",
                    sqlMapper.remapSqlName(table.getTableSchema()),
                    sqlMapper.remapSqlName(table.getTableName()),
                    remapSourceColumn.getColumnName()
            ));
        }
        if (!remapSourceColumn.isNullable() && targetColumn.isNullable()) {
            sqlCommands.add(String.format(
                    "ALTER TABLE \"%s\".\"%s\" ALTER COLUMN \"%s\" SET NOT NULL",
                    sqlMapper.remapSqlName(table.getTableSchema()),
                    sqlMapper.remapSqlName(table.getTableName()),
                    remapSourceColumn.getColumnName()
            ));
        }
        if (!Objects.equals(remapSourceColumn.getDataType(), targetColumn.getDataType())
                || !Objects.equals(remapSourceColumn.getMaxLength(), targetColumn.getMaxLength())
        ) {
            sqlCommands.add(String.format(
                    "ALTER TABLE \"%s\".\"%s\" ALTER COLUMN \"%s\" SET DATA TYPE %s",
                    sqlMapper.remapSqlName(table.getTableSchema()),
                    sqlMapper.remapSqlName(table.getTableName()),
                    remapSourceColumn.getColumnName(),
                    columnDataType(remapSourceColumn)
            ));
        }
        return sqlCommands;
    }

    @Override
    public String dropColumn(Table table, Column column) {
        return String.format(
                "ALTER TABLE \"%s\".\"%s\" DROP COLUMN \"%s\"",
                sqlMapper.remapSqlName(table.getTableSchema()),
                sqlMapper.remapSqlName(table.getTableName()),
                sqlMapper.remapSqlName(column.getColumnName())
        );
    }

    private String columnsSqlDefinition(Table table) {
        return table.getColumns().stream()
                .map(this::columnSqlDefinition)
                .collect(Collectors.joining(", "));
    }

    private String columnSqlDefinition(Column column) {
        final Column remapColumn = sqlMapper.remapColumn(column);
        if (Objects.isNull(remapColumn.getMaxLength())
                || MAX_COLUMN_LENGTH_INT_MARKER == remapColumn.getMaxLength()
                || CHARACTER_COLUMNS_WITHOUT_LENGTH.contains(remapColumn.getDataType().toLowerCase())) {
            return columnWithoutLength(remapColumn);
        } else {
            return columnWithLength(remapColumn);
        }
    }

    private String columnWithoutLength(Column column) {
        return String.format(
                "\"%s\" %s %s",
                column.getColumnName(),
                column.getDataType(),
                nullability(column)
        );
    }

    private String columnWithLength(Column column) {
        return String.format(
                "\"%s\" %s(%s) %s",
                column.getColumnName(),
                column.getDataType(),
                column.getMaxLength(),
                nullability(column)
        );
    }

    private String nullability(Column column) {
        final String nullability;
        if (column.isNullable()) {
            nullability = NULLABLE_COLUMN_DEFINITION;
        } else {
            nullability = NON_NULLABLE_COLUMN_DEFINITION;
        }
        return nullability;
    }

    private String columnNamesList(List<Column> columns) {
        return columns.stream()
                .map(Column::getColumnName)
                .map(sqlMapper::remapSqlName)
                .map(col -> String.format("\"%s\"", col))
                .collect(Collectors.joining(", "));
    }

    private String insertParameters(List<Column> columns) {
        return Stream.generate(() -> "?")
                .limit(columns.size())
                .collect(Collectors.joining(", "));
    }

    private String updateColumnsSet(PrimaryKey primaryKey, List<Column> columns) {
        return columns.stream()
                .map(Column::getColumnName)
                .filter(columnNamesToUpdate(primaryKey, columns))
                .map(sqlMapper::remapSqlName)
                .map(col -> String.format("\"%s\" = ?", col))
                .collect(Collectors.joining(", "));
    }

    private String whereColumnsSet(List<Column> columns) {
        return columns.stream()
                .map(Column::getColumnName)
                .map(sqlMapper::remapSqlName)
                .map(col -> String.format("\"%s\" = ?", col))
                .collect(Collectors.joining(" AND "));
    }

    private String columnDataType(Column column) {
        if (Objects.isNull(column.getMaxLength())
                || MAX_COLUMN_LENGTH_INT_MARKER == column.getMaxLength()
                || CHARACTER_COLUMNS_WITHOUT_LENGTH.contains(column.getDataType().toLowerCase())) {
            return String.format("%s", column.getDataType());
        } else {
            return String.format("%s(%s)", column.getDataType(), column.getMaxLength());
        }
    }

    private String mergeColumnNamesList(List<Column> columns) {
        return columns.stream()
                .map(Column::getColumnName)
                .map(sqlMapper::remapSqlName)
                .map(col -> String.format("? AS \"%s\"", col))
                .collect(Collectors.joining(", "));
    }

    private String mergeJoinColumnsSet(List<Column> columns) {
        return columns.stream()
                .map(Column::getColumnName)
                .map(sqlMapper::remapSqlName)
                .map(col -> String.format("sourceData.\"%s\" = targetTable.\"%s\"", col, col))
                .collect(Collectors.joining(" AND "));
    }
}
