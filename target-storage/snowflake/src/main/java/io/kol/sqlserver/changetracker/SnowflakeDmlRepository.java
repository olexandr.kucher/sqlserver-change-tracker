package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.sql.AbstractDmlRepository;
import io.kol.sqlserver.changetracker.sql.SqlDefinitionCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

class SnowflakeDmlRepository extends AbstractDmlRepository {
    SnowflakeDmlRepository(NamedParameterJdbcTemplate jdbcTemplate,
                           SqlDefinitionCreator sqlDefinitionCreator) {
        super(jdbcTemplate, sqlDefinitionCreator);
    }
}
