package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.sql.SqlMapper;

class SnowflakeSqlMapper implements SqlMapper {
    @Override
    public String remapSqlName(String sqlName) {
        return sqlName.toUpperCase();
    }

    @Override
    public Column remapColumn(Column column) {
        return Column.builder()
                .setPosition(column.getPosition())
                .setColumnName(remapSqlName(column.getColumnName()))
                .setDataType(remapDataType(column.getDataType()))
                .setMaxLength(column.getMaxLength())
                .setNullable(column.isNullable())
                .setCollationName(column.getCollationName())
                .build();
    }

    private String remapDataType(String dataType) {
        return switch (dataType.toUpperCase()) {
            case "DATE" -> "DATE";
            case "TIME" -> "TIME";
            case "BIT" -> "BOOLEAN";
            case "BINARY", "VARBINARY", "IMAGE" -> "BINARY";
            case "BIGINT", "SMALLINT", "INT", "TINYINT" -> "NUMBER";
            case "VARCHAR", "TEXT", "NVARCHAR", "NTEXT", "CHAR", "NCHAR", "TIMESTAMP", "ROWVERSION" -> "TEXT";
            case "FLOAT", "REAL", "SMALLMONEY", "MONEY", "NUMERIC", "DECIMAL" -> "FLOAT";
            case "DATETIMEOFFSET", "DATETIME2", "SMALLDATETIME", "DATETIME" -> "TIMESTAMP_NTZ";
            default -> "OBJECT";
        };
    }
}
