package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;
import io.kol.sqlserver.changetracker.sql.DdlRepository;
import io.kol.sqlserver.changetracker.sql.SqlDefinitionCreator;
import io.kol.sqlserver.changetracker.sql.SqlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SnowflakeDdlRepository implements DdlRepository {
    private static final String IS_NULLABLE_COLUMN_MARKER = "YES";

    private final Logger logger = LoggerFactory.getLogger(SnowflakeDdlRepository.class);

    private final SqlMapper sqlMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlDefinitionCreator sqlDefinitionCreator;

    SnowflakeDdlRepository(SqlMapper sqlMapper,
                           NamedParameterJdbcTemplate jdbcTemplate,
                           SqlDefinitionCreator sqlDefinitionCreator) {
        this.sqlMapper = sqlMapper;
        this.jdbcTemplate = jdbcTemplate;
        this.sqlDefinitionCreator = sqlDefinitionCreator;
    }

    @Override
    public List<TargetTable> getAllTablesInSchema(String schemaName) {
        final String sql = """
                    SELECT "TABLE_SCHEMA", "TABLE_NAME"
                    FROM "INFORMATION_SCHEMA"."TABLES"
                    WHERE "TABLE_SCHEMA" = :table_schema
                """;
        return jdbcTemplate.query(
                sql, Map.of("table_schema", sqlMapper.remapSqlName(schemaName)),
                (rs, i) -> new TargetTable(
                        rs.getString("TABLE_SCHEMA"),
                        rs.getString("TABLE_NAME")
                )
        );
    }

    @Override
    public Map<TargetTable, List<Column>> getAllTableColumns() {
        final String sql = """
                    SELECT "TABLE_SCHEMA", "TABLE_NAME", "ORDINAL_POSITION", "COLUMN_NAME", "DATA_TYPE",
                     "CHARACTER_MAXIMUM_LENGTH", "IS_NULLABLE", "COLLATION_NAME"
                    FROM "INFORMATION_SCHEMA"."COLUMNS"
                """;
        final Map<TargetTable, List<Column>> map = new HashMap<>();
        jdbcTemplate.query(sql, rs -> {
            final TargetTable table = new TargetTable(
                    rs.getString("TABLE_SCHEMA"),
                    rs.getString("TABLE_NAME")
            );
            final BigDecimal maximumLength = (BigDecimal) rs.getObject("CHARACTER_MAXIMUM_LENGTH");
            final Column column = Column
                    .builder()
                    .setPosition(rs.getInt("ORDINAL_POSITION"))
                    .setColumnName(rs.getString("COLUMN_NAME"))
                    .setDataType(rs.getString("DATA_TYPE"))
                    .setMaxLength(null == maximumLength ? null : maximumLength.intValue())
                    .setNullable(IS_NULLABLE_COLUMN_MARKER.equalsIgnoreCase(rs.getString("IS_NULLABLE")))
                    .setCollationName(rs.getString("COLLATION_NAME"))
                    .build();
            map.computeIfAbsent(table, key -> new ArrayList<>()).add(column);
        });
        return map;
    }

    @Override
    public boolean isSchemaExists(String schema) {
        final String sql = """
                            SELECT cast(count(*) AS BOOLEAN)
                            FROM "INFORMATION_SCHEMA"."SCHEMATA"
                            WHERE "SCHEMA_NAME" = :schema_name
                """;
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of(
                "schema_name", sqlMapper.remapSqlName(schema)
        ), boolean.class);
    }

    @Override
    public void createSchema(String schema) {
        final String sql = sqlDefinitionCreator.createSchema(schema);
        logger.debug("Execute create schema: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void dropSchema(String schema) {
        final String sql = sqlDefinitionCreator.dropSchema(schema);
        logger.debug("Execute drop schema: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void createTable(Table table) {
        final String sql = sqlDefinitionCreator.createTable(table);
        logger.debug("Execute create table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void dropTable(TargetTable table) {
        final String sql = sqlDefinitionCreator.dropTable(table);
        logger.debug("Execute drop table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void createColumn(Table table, Column column) {
        final String sql = sqlDefinitionCreator.createColumn(table, column);
        logger.debug("Execute alter table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    @Transactional(transactionManager = "targetStorageTransactionalManager")
    public void alterColumn(Table table, Column sourceColumn, Column targetColumn) {
        final List<String> sqlCommands = sqlDefinitionCreator.alterColumn(table, sourceColumn, targetColumn);
        logger.debug("Execute alter table: {}", sqlCommands);
        sqlCommands.forEach(sql -> jdbcTemplate.update(sql, Map.of()));
    }

    @Override
    public void dropColumn(Table table, Column column) {
        final String sql = sqlDefinitionCreator.dropColumn(table, column);
        logger.debug("Execute alter table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }
}
