package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.sql.SqlMapper;
import io.kol.sqlserver.changetracker.sync.ColumnEqualComparator;

import java.util.Objects;

class SnowflakeColumnEqualComparator implements ColumnEqualComparator {
    private final SqlMapper sqlMapper;

    SnowflakeColumnEqualComparator(SqlMapper sqlMapper) {
        this.sqlMapper = sqlMapper;
    }

    @Override
    public boolean isEquals(Column sourceColumn, Column targetColumn) {
        final Column remapSourceColumn = sqlMapper.remapColumn(sourceColumn);
        return sourceColumn.isNullable() == targetColumn.isNullable()
                && Objects.equals(remapSourceColumn.getColumnName(), targetColumn.getColumnName())
                && Objects.equals(remapSourceColumn.getDataType(), targetColumn.getDataType())
                && Objects.equals(sourceColumn.getMaxLength(), targetColumn.getMaxLength());
    }
}
