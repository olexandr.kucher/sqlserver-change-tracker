package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.PrimaryKeyValue;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.lang.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

class DeleteBatchPreparedStatementSetter implements BatchPreparedStatementSetter {
    private final Table table;
    private final List<PrimaryKeyValue> primaryKeyValues;

    DeleteBatchPreparedStatementSetter(Table table, List<PrimaryKeyValue> primaryKeyValues) {
        this.table = table;
        this.primaryKeyValues = primaryKeyValues;
    }

    @Override
    public void setValues(@NonNull PreparedStatement ps, int i) throws SQLException {
        final PrimaryKeyValue primaryKeyValue = primaryKeyValues.get(i);
        final List<Column> columns = table.getPrimaryKey().getColumns();
        for (int position = 0; position < columns.size(); position++) {
            ps.setObject(
                    position + 1,
                    primaryKeyValue.getValues()[position],
                    columns.get(position).getJavaSqlType()
            );
        }
    }

    @Override
    public int getBatchSize() {
        return primaryKeyValues.size();
    }
}
