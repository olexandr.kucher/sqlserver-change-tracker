package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.lang.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.kol.sqlserver.changetracker.model.PrimaryKey.columnsToUpdate;

class MergeBatchPreparedStatementSetter implements BatchPreparedStatementSetter {
    private final Table table;
    private final List<Map<String, Object>> data;
    private final List<Column> nonPrimaryKeyColumns;

    MergeBatchPreparedStatementSetter(Table table, List<Map<String, Object>> data) {
        this.table = table;
        this.data = data;
        this.nonPrimaryKeyColumns = table.getColumns().stream()
                .filter(columnsToUpdate(table.getPrimaryKey(), table.getColumns()))
                .sorted(Comparator.comparingInt(Column::getPosition))
                .collect(Collectors.toList());
    }

    @Override
    public void setValues(@NonNull PreparedStatement ps, int i) throws SQLException {
        final Map<String, Object> map = data.get(i);
        final List<Column> columns = table.getColumns();
        int position = 1;
        for (Column column : columns) {
            ps.setObject(position, map.get(column.getColumnName()), column.getJavaSqlType());
            position++;
        }

        for (Column column : nonPrimaryKeyColumns) {
            ps.setObject(position, map.get(column.getColumnName()), column.getJavaSqlType());
            position++;
        }

        for (Column column : columns) {
            ps.setObject(position, map.get(column.getColumnName()), column.getJavaSqlType());
            position++;
        }
    }

    @Override
    public int getBatchSize() {
        return data.size();
    }
}
