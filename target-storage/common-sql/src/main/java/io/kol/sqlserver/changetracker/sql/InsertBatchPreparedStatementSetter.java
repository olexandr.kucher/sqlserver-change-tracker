package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.lang.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

class InsertBatchPreparedStatementSetter implements BatchPreparedStatementSetter {
    private final Table table;
    private final List<Map<String, Object>> data;

    InsertBatchPreparedStatementSetter(Table table, List<Map<String, Object>> data) {
        this.table = table;
        this.data = data;
    }

    @Override
    public void setValues(@NonNull PreparedStatement ps, int i) throws SQLException {
        final Map<String, Object> map = data.get(i);
        final List<Column> columns = table.getColumns();
        for (int position = 0; position < columns.size(); position++) {
            final Column column = columns.get(position);
            ps.setObject(
                    position + 1,
                    map.get(column.getColumnName()),
                    column.getJavaSqlType()
            );
        }
    }

    @Override
    public int getBatchSize() {
        return data.size();
    }
}
