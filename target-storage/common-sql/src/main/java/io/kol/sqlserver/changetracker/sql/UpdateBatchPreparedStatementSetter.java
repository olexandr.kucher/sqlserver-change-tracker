package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.PrimaryKey;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.lang.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.kol.sqlserver.changetracker.model.PrimaryKey.columnsToUpdate;

class UpdateBatchPreparedStatementSetter implements BatchPreparedStatementSetter {
    private final List<Column> columns;
    private final PrimaryKey primaryKey;
    private final List<Map<String, Object>> data;

    UpdateBatchPreparedStatementSetter(Table table, List<Map<String, Object>> data) {
        this.data = data;
        this.primaryKey = table.getPrimaryKey();
        this.columns = table.getColumns().stream()
                .filter(columnsToUpdate(primaryKey, table.getColumns()))
                .sorted(Comparator.comparingInt(Column::getPosition))
                .collect(Collectors.toList());
    }

    @Override
    public void setValues(@NonNull PreparedStatement ps, int i) throws SQLException {
        final Map<String, Object> map = data.get(i);

        int position = 1;
        for (Column column : columns) {
            ps.setObject(position, map.get(column.getColumnName()), column.getJavaSqlType());
            position++;
        }

        final List<Column> primaryKeyColumns = primaryKey.getColumns();
        for (Column column : primaryKeyColumns) {
            ps.setObject(position, map.get(column.getColumnName()), column.getJavaSqlType());
            position++;
        }
    }

    @Override
    public int getBatchSize() {
        return data.size();
    }
}
