package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.PrimaryKeyValue;
import io.kol.sqlserver.changetracker.model.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

public class AbstractDmlRepository implements DmlRepository {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlDefinitionCreator sqlDefinitionCreator;

    protected AbstractDmlRepository(NamedParameterJdbcTemplate jdbcTemplate,
                          SqlDefinitionCreator sqlDefinitionCreator) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlDefinitionCreator = sqlDefinitionCreator;
    }

    @Override
    public void truncate(Table table) {
        final String sql = sqlDefinitionCreator.truncate(table);
        logger.debug("Execute truncate: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void create(Table table, List<Map<String, Object>> data) {
        if (data.isEmpty()) {
            return;
        }

        final String sql = sqlDefinitionCreator.insert(table);
        logger.debug("Execute insert (size={}): {}", data.size(), sql);
        jdbcTemplate.getJdbcOperations().batchUpdate(sql, new InsertBatchPreparedStatementSetter(table, data));
    }

    @Override
    public void merge(Table table, List<Map<String, Object>> data) {
        if (data.isEmpty()) {
            return;
        }

        final String sql = sqlDefinitionCreator.merge(table);
        logger.debug("Execute merge (size={}): {}", data.size(), sql);
        jdbcTemplate.getJdbcOperations().batchUpdate(sql, new MergeBatchPreparedStatementSetter(table, data));
    }

    @Override
    public void update(Table table, List<Map<String, Object>> data) {
        if (data.isEmpty()) {
            return;
        }

        final String sql = sqlDefinitionCreator.update(table);
        logger.debug("Execute update (size={}): {}", data.size(), sql);
        jdbcTemplate.getJdbcOperations().batchUpdate(sql, new UpdateBatchPreparedStatementSetter(table, data));
    }

    @Override
    public void delete(Table table, List<PrimaryKeyValue> primaryKeyValues) {
        if (primaryKeyValues.isEmpty()) {
            return;
        }

        final String sql = sqlDefinitionCreator.delete(table);
        logger.debug("Execute delete (size={}): {}", primaryKeyValues.size(), sql);
        jdbcTemplate.getJdbcOperations().batchUpdate(sql, new DeleteBatchPreparedStatementSetter(table, primaryKeyValues));
    }
}
