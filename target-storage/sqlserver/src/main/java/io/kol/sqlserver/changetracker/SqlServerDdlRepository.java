package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;
import io.kol.sqlserver.changetracker.sql.DdlRepository;
import io.kol.sqlserver.changetracker.sql.SqlDefinitionCreator;
import io.kol.sqlserver.changetracker.sql.SqlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SqlServerDdlRepository implements DdlRepository {
    private static final String IS_NULLABLE_COLUMN_MARKER = "YES";

    private final Logger logger = LoggerFactory.getLogger(SqlServerDdlRepository.class);

    private final SqlMapper sqlMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlDefinitionCreator sqlDefinitionCreator;

    SqlServerDdlRepository(SqlMapper sqlMapper,
                           NamedParameterJdbcTemplate jdbcTemplate,
                           SqlDefinitionCreator sqlDefinitionCreator) {
        this.sqlMapper = sqlMapper;
        this.jdbcTemplate = jdbcTemplate;
        this.sqlDefinitionCreator = sqlDefinitionCreator;
    }

    @Override
    public List<TargetTable> getAllTablesInSchema(String schemaName) {
        final String sql = """
                    SELECT s.name AS tableSchema, t.name AS tableName
                    FROM sys.tables t
                    JOIN sys.schemas s ON s.schema_id = t.schema_id
                    WHERE s.name = :table_schema
                """;
        return jdbcTemplate.query(
                sql, Map.of("table_schema", sqlMapper.remapSqlName(schemaName)),
                (rs, i) -> new TargetTable(
                        rs.getString("tableSchema"),
                        rs.getString("tableName")
                )
        );
    }

    @Override
    public Map<TargetTable, List<Column>> getAllTableColumns() {
        final String sql = """
                    SELECT
                     s.name AS schemaName, OBJECT_NAME(t.object_id) AS tableName,
                     col.ORDINAL_POSITION, col.COLUMN_NAME, col.DATA_TYPE,
                     col.CHARACTER_MAXIMUM_LENGTH, col.IS_NULLABLE, col.COLLATION_NAME
                    FROM sys.tables t
                        JOIN sys.schemas s ON s.schema_id = t.schema_id
                        JOIN INFORMATION_SCHEMA.COLUMNS col ON col.TABLE_SCHEMA = s.name AND col.TABLE_NAME = OBJECT_NAME(t.object_id);
                """;
        final Map<TargetTable, List<Column>> map = new HashMap<>();
        jdbcTemplate.query(sql, rs -> {
            final TargetTable table = new TargetTable(
                    rs.getString("schemaName"),
                    rs.getString("tableName")
            );
            final Column column = Column
                    .builder()
                    .setPosition(rs.getInt("ORDINAL_POSITION"))
                    .setColumnName(rs.getString("COLUMN_NAME"))
                    .setDataType(rs.getString("DATA_TYPE"))
                    .setMaxLength((Integer) rs.getObject("CHARACTER_MAXIMUM_LENGTH"))
                    .setNullable(IS_NULLABLE_COLUMN_MARKER.equalsIgnoreCase(rs.getString("IS_NULLABLE")))
                    .setCollationName(rs.getString("COLLATION_NAME"))
                    .build();
            map.computeIfAbsent(table, key -> new ArrayList<>()).add(column);
        });
        return map;
    }

    @Override
    public boolean isSchemaExists(String schema) {
        final String sql = "SELECT CAST(count(*) AS BIT) FROM sys.schemas WHERE name = :schema_name";
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of(
                "schema_name", sqlMapper.remapSqlName(schema)
        ), boolean.class);
    }

    @Override
    public void createSchema(String schema) {
        final String sql = sqlDefinitionCreator.createSchema(schema);
        logger.debug("Execute create schema: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void dropSchema(String schema) {
        final String sql = sqlDefinitionCreator.dropSchema(schema);
        logger.debug("Execute drop schema: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void createTable(Table table) {
        final String sql = sqlDefinitionCreator.createTable(table);
        logger.debug("Execute create table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void dropTable(TargetTable table) {
        final String sql = sqlDefinitionCreator.dropTable(table);
        logger.debug("Execute drop table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    public void createColumn(Table table, Column column) {
        final String sql = sqlDefinitionCreator.createColumn(table, column);
        logger.debug("Execute alter table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }

    @Override
    @Transactional(transactionManager = "targetStorageTransactionalManager")
    public void alterColumn(Table table, Column sourceColumn, Column targetColumn) {
        final List<String> sqlCommands = sqlDefinitionCreator.alterColumn(table, sourceColumn, targetColumn);
        logger.debug("Execute alter table: {}", sqlCommands);
        sqlCommands.forEach(sql -> jdbcTemplate.update(sql, Map.of()));
    }

    @Override
    public void dropColumn(Table table, Column column) {
        final String sql = sqlDefinitionCreator.dropColumn(table, column);
        logger.debug("Execute alter table: {}", sql);
        jdbcTemplate.update(sql, Map.of());
    }
}
