package io.kol.sqlserver.changetracker;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.kol.sqlserver.changetracker.annotation.TargetStorage;
import io.kol.sqlserver.changetracker.property.DatabaseProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@ConditionalOnProperty(name = "target.storage.type", havingValue = "SQLServer")
class SqlServerTargetDatabaseConfig {
    @ConfigurationProperties("target.storage")
    @TargetStorage
    @Bean
    DatabaseProperties targetStorageProperties() {
        return new DatabaseProperties();
    }

    @TargetStorage
    @Bean(destroyMethod = "close")
    HikariDataSource targetDatabaseDataSource(@TargetStorage DatabaseProperties properties) {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getJdbcUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        config.setDriverClassName(com.microsoft.sqlserver.jdbc.SQLServerDriver.class.getName());
        return new HikariDataSource(config);
    }

    @TargetStorage
    @Bean
    NamedParameterJdbcTemplate targetDatabaseJdbcTemplate(@TargetStorage HikariDataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @TargetStorage
    @Bean
    PlatformTransactionManager targetStorageTransactionalManager(@TargetStorage HikariDataSource dataSource) {
        final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        transactionManager.setGlobalRollbackOnParticipationFailure(false);
        return transactionManager;
    }

    @Bean
    SqlServerSqlMapper sqlServerSqlMapper() {
        return new SqlServerSqlMapper();
    }

    @Bean
    SqlServerSqlDefinitionCreator sqlServerSqlDefinitionCreator(SqlServerSqlMapper sqlServerSqlColumnMapper) {
        return new SqlServerSqlDefinitionCreator(sqlServerSqlColumnMapper);
    }

    @Bean
    SqlServerDmlRepository sqlServerDmlRepository(@TargetStorage NamedParameterJdbcTemplate jdbcTemplate,
                                                  SqlServerSqlDefinitionCreator sqlServerSqlDefinitionCreator) {
        return new SqlServerDmlRepository(jdbcTemplate, sqlServerSqlDefinitionCreator);
    }

    @Bean
    SqlServerDdlRepository sqlServerDdlRepository(SqlServerSqlMapper sqlServerSqlColumnMapper,
                                                  @TargetStorage NamedParameterJdbcTemplate jdbcTemplate,
                                                  SqlServerSqlDefinitionCreator sqlServerSqlDefinitionCreator) {
        return new SqlServerDdlRepository(sqlServerSqlColumnMapper, jdbcTemplate, sqlServerSqlDefinitionCreator);
    }

    @Bean
    SqlServerColumnEqualComparator sqlServerColumnEqualComparator(SqlServerSqlMapper sqlServerSqlColumnMapper) {
        return new SqlServerColumnEqualComparator(sqlServerSqlColumnMapper);
    }
}
