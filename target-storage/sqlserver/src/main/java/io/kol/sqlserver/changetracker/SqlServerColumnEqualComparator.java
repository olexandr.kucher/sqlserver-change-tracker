package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.sql.SqlMapper;
import io.kol.sqlserver.changetracker.sync.ColumnEqualComparator;

import java.util.Objects;

class SqlServerColumnEqualComparator implements ColumnEqualComparator {
    private final SqlMapper sqlMapper;

    SqlServerColumnEqualComparator(SqlMapper sqlMapper) {
        this.sqlMapper = sqlMapper;
    }

    @Override
    public boolean isEquals(Column sourceColumn, Column targetColumn) {
        final Column remapSourceColumn = sqlMapper.remapColumn(sourceColumn);
        return Objects.equals(remapSourceColumn, targetColumn);
    }
}
