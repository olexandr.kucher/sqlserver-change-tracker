package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.sql.SqlMapper;

class SqlServerSqlMapper implements SqlMapper {
    @Override
    public String remapSqlName(String sqlName) {
        return sqlName;
    }

    @Override
    public Column remapColumn(Column column) {
        final String dataType = column.getDataType();
        final String remapDataType = switch (dataType.toLowerCase()) {
            case VERSION_COLUMN_TYPE_VERSION, VERSION_COLUMN_TYPE_TIMESTAMP -> VERSION_COLUMN_REMAP_TYPE_NAME;
            default -> dataType;
        };

        final Integer remapMaxLength = switch (dataType.toLowerCase()) {
            case VERSION_COLUMN_TYPE_VERSION, VERSION_COLUMN_TYPE_TIMESTAMP -> VERSION_COLUMN_REMAP_TYPE_LENGTH;
            default -> column.getMaxLength();
        };

        return Column.builder()
                .setPosition(column.getPosition())
                .setColumnName(remapSqlName(column.getColumnName()))
                .setDataType(remapDataType)
                .setMaxLength(remapMaxLength)
                .setNullable(column.isNullable())
                .setCollationName(column.getCollationName())
                .build();
    }
}
