create table tracked_table (
    id bigserial not null,
    schema_name varchar(30) not null,
    table_name varchar(100) not null,
    primary_key jsonb not null,
    columns jsonb not null,
    constraint tracked_table_pk primary key (id),
    constraint tracked_table_schema_name_table_name_uq unique (schema_name, table_name)
)