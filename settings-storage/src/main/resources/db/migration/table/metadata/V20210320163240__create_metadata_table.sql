create table metadata (
    type varchar(50) not null,
    value jsonb not null,
    time timestamp not null,
    constraint metadata_pk primary key (type)
)