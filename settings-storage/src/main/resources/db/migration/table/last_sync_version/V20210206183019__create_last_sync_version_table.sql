create table if not exists last_sync_data (
    schema_name varchar(30) not null,
    table_name varchar(100) not null,
    sync_version bigint not null,
    sync_time timestamp not null,
    constraint last_sync_data_pk primary key (schema_name, table_name)
)