package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Metadata;
import io.kol.sqlserver.changetracker.model.MetadataType;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
class SettingsServiceImpl implements SettingsService {
    private static final int NO_FAILURES_MARKER = 0;
    private static final long FULL_REINIT_VERSION_MARKER = -1L;

    private final Logger logger = LoggerFactory.getLogger(SettingsServiceImpl.class);

    private final SettingsRepository settingsRepository;
    private final DateTimeProvider dateTimeProvider;
    private final int maxSyncFailuresCount;

    SettingsServiceImpl(SyncDataProperties syncDataProperties,
                        SettingsRepository settingsRepository,
                        DateTimeProvider dateTimeProvider) {
        this.maxSyncFailuresCount = syncDataProperties.getMaxSyncFailuresCount();
        this.settingsRepository = settingsRepository;
        this.dateTimeProvider = dateTimeProvider;
    }

    @Override
    public Metadata getMetadata(MetadataType type) {
        return settingsRepository.getMetadata(type)
                .orElse(type.emptyMetadata(dateTimeProvider.getNow()));
    }

    @Override
    public void saveMetadata(MetadataType type, Object value) {
        settingsRepository.saveMetadata(type, value, dateTimeProvider.getNow());
    }

    @Override
    public long getLastSyncVersion(Table table) {
        return getLastSyncData(table).syncVersion();
    }

    @Override
    public List<Table> getAllTrackedTables() {
        return settingsRepository.getAllTrackedTables()
                .stream()
                .sorted(Comparator
                        .comparing(Table::getTableSchema)
                        .thenComparing(Table::getTableName)
                ).collect(Collectors.toList());
    }

    @Override
    public void saveLastSyncVersion(Table table, long lastSyncVersion) {
        settingsRepository.saveLastSyncVersion(table, lastSyncVersion, NO_FAILURES_MARKER, dateTimeProvider.getNow());
    }

    @Override
    @Transactional(transactionManager = "settingsStorageTransactionalManager")
    public void saveTrackedTables(List<Table> tables) {
        logger.debug("Deleting all tracked tables from settings storage...");
        settingsRepository.deleteTrackedTables();
        logger.debug("Saving new tracked tables into settings storage: {}", tables);
        settingsRepository.saveTrackedTables(tables);
        logger.debug("Deleting sync data for all untracked tables from  settings storage...");
        settingsRepository.deleteSyncDataForUntrackedTables();
        logger.debug("Tracked tables successfully recreated in settings storage: {}", tables);
    }

    @Override
    public void markForFullReInit(Table table) {
        final LastSyncData data = getLastSyncData(table);
        settingsRepository.saveLastSyncVersion(table, FULL_REINIT_VERSION_MARKER, data.failuresCount(), dateTimeProvider.getNow());
    }

    @Override
    public void markForFullReInitIfRequired(Table table) {
        final LastSyncData data = getLastSyncData(table);
        if (data.failuresCount() >= maxSyncFailuresCount) {
            settingsRepository.saveLastSyncVersion(table, FULL_REINIT_VERSION_MARKER, NO_FAILURES_MARKER, dateTimeProvider.getNow());
        } else {
            settingsRepository.saveLastSyncVersion(table, data.syncVersion(), data.failuresCount() + 1, dateTimeProvider.getNow());
        }
    }

    @Override
    public void deleteLastSyncData() {
        settingsRepository.deleteLastSyncData();
    }

    private LastSyncData getLastSyncData(Table table) {
        return settingsRepository.getLastSyncVersion(table)
                .orElseGet(() -> new LastSyncData(FULL_REINIT_VERSION_MARKER, NO_FAILURES_MARKER));
    }
}
