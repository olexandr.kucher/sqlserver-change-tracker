package io.kol.sqlserver.changetracker;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.kol.sqlserver.changetracker.annotation.SettingsStorage;
import io.kol.sqlserver.changetracker.property.DatabaseProperties;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
class SettingsDatabaseConfig {
    @ConfigurationProperties("settings.database")
    @SettingsStorage
    @Bean
    DatabaseProperties settingsStorageProperties() {
        return new DatabaseProperties();
    }

    @SettingsStorage
    @FlywayDataSource
    @Bean(destroyMethod = "close")
    HikariDataSource settingsStorageDataSource(@SettingsStorage DatabaseProperties properties) {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getJdbcUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        config.setDriverClassName(org.postgresql.Driver.class.getName());
        return new HikariDataSource(config);
    }

    @SettingsStorage
    @Bean
    NamedParameterJdbcTemplate settingsStorageJdbcTemplate(@SettingsStorage HikariDataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @SettingsStorage
    @Bean
    PlatformTransactionManager settingsStorageTransactionalManager(@SettingsStorage HikariDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @SettingsStorage
    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
}
