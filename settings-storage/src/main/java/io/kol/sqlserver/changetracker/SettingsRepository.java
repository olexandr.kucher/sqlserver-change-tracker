package io.kol.sqlserver.changetracker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kol.sqlserver.changetracker.annotation.SettingsStorage;
import io.kol.sqlserver.changetracker.model.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
class SettingsRepository {
    private final ObjectMapper objectMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    SettingsRepository(@SettingsStorage ObjectMapper objectMapper,
                       @SettingsStorage NamedParameterJdbcTemplate jdbcTemplate) {
        this.objectMapper = objectMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    Optional<Metadata> getMetadata(MetadataType type) {
        final String sql = "SELECT value, time FROM metadata WHERE type = :type";
        return jdbcTemplate.query(
                sql, Map.of("type", type.name()),
                (rs, i) -> new Metadata(
                        type,
                        type.readValueFromJson(objectMapper, rs.getString("value")),
                        rs.getTimestamp("time").toLocalDateTime()
                )).stream().findFirst();
    }

    void saveMetadata(MetadataType type, Object value, LocalDateTime timestamp) {
        final String sql = """
                    INSERT INTO metadata(type, value, time)
                    VALUES (:type, to_json(:value::json), :time)
                    ON CONFLICT ON CONSTRAINT metadata_pk
                    DO UPDATE SET value = to_json(:value::json), time = :time
                """;
        jdbcTemplate.update(sql, Map.of(
                "type", type.name(),
                "value", writeDataAsJson(value),
                "time", timestamp
        ));
    }

    Optional<LastSyncData> getLastSyncVersion(Table table) {
        final String sql = """
                    SELECT sync_version, sync_time, failures_count
                    FROM last_sync_data
                    WHERE schema_name = :schema_name AND table_name = :table_name
                """;
        return jdbcTemplate.query(sql, Map.of(
                "schema_name", table.getTableSchema(),
                "table_name", table.getTableName()
        ), (rs, i) -> new LastSyncData(
                rs.getLong("sync_version"),
                rs.getInt("failures_count")
        )).stream().findFirst();
    }

    List<Table> getAllTrackedTables() {
        final String sql = "SELECT schema_name, table_name, primary_key, columns FROM tracked_table";
        return jdbcTemplate.query(sql, (rs, i) -> Table.builder()
                .setTableSchema(rs.getString("schema_name"))
                .setTableName(rs.getString("table_name"))
                .setPrimaryKey(new PrimaryKey(readColumnsFromJson(rs.getString("primary_key"))))
                .setColumns(readColumnsFromJson(rs.getString("columns")))
                .build()
        );
    }

    void saveLastSyncVersion(Table table, long lastSyncVersion, int failuresCount, LocalDateTime timestamp) {
        final String sql = """
                    INSERT INTO last_sync_data(schema_name, table_name, sync_version, sync_time, failures_count)
                    VALUES (:schema_name, :table_name, :last_sync_version, :last_sync_timestamp, :failures_count)
                    ON CONFLICT ON CONSTRAINT last_sync_data_pk
                    DO UPDATE SET sync_version = :last_sync_version, sync_time = :last_sync_timestamp, failures_count = :failures_count
                """;
        jdbcTemplate.update(sql, Map.of(
                "schema_name", table.getTableSchema(),
                "table_name", table.getTableName(),
                "last_sync_version", lastSyncVersion,
                "last_sync_timestamp", timestamp,
                "failures_count", failuresCount
        ));
    }

    void saveTrackedTables(List<Table> tables) {
        final String sql = """
                    INSERT INTO tracked_table(schema_name, table_name, primary_key, columns)
                    VALUES (:schema_name, :table_name, to_json(:primary_key::json), to_json(:columns::json))
                    ON CONFLICT ON CONSTRAINT tracked_table_schema_name_table_name_uq
                    DO UPDATE SET primary_key = to_json(:primary_key::json), columns = to_json(:columns::json)
                """;
        jdbcTemplate.batchUpdate(sql, tables.stream()
                .map(table -> new MapSqlParameterSource()
                        .addValue("schema_name", table.getTableSchema())
                        .addValue("table_name", table.getTableName())
                        .addValue("primary_key", writeDataAsJson(table.getPrimaryKey().getColumns()))
                        .addValue("columns", writeDataAsJson(table.getColumns()))
                ).toArray(MapSqlParameterSource[]::new));
    }

    void deleteTrackedTables() {
        final String sql = "TRUNCATE TABLE tracked_table";
        jdbcTemplate.update(sql, Map.of());
    }

    void deleteLastSyncData() {
        final String sql = "TRUNCATE TABLE last_sync_data";
        jdbcTemplate.update(sql, Map.of());
    }

    void deleteSyncDataForUntrackedTables() {
        final String sql = """
                    DELETE FROM last_sync_data lsd
                    WHERE NOT EXISTS(SELECT 1
                        FROM tracked_table tt
                        WHERE tt.schema_name = lsd.schema_name AND tt.table_name = lsd.table_name
                    )
                """;
        jdbcTemplate.update(sql, Map.of());
    }

    private List<Column> readColumnsFromJson(String json) {
        try {
            return objectMapper.readValue(json, new TypeReference<>() {});
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Cannot parse columns json.", e);
        }
    }

    private <T> String writeDataAsJson(T data) {
        try {
            return objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Cannot convert data to json.", e);
        }
    }
}
