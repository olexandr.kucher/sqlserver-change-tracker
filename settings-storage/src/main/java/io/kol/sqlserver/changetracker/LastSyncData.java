package io.kol.sqlserver.changetracker;

public record LastSyncData(long syncVersion, int failuresCount) {
}