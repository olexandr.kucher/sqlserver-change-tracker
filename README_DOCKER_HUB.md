# SQL Server Change Tracker

Simple Java application replicates data from Microsoft SQL Server database
using [Change Tracking mechanism](https://docs.microsoft.com/en-us/sql/relational-databases/track-changes/about-change-tracking-sql-server?view=sql-server-ver15)
and loads it into one of the supported data warehouses:
* [Microsoft SQL Server](https://docs.microsoft.com/en-us/sql/sql-server/?view=sql-server-ver15) (**type=SQLServer**)
* [Snowflake](https://www.snowflake.com) (**type=Snowflake**)

See [project at GitLab](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker) for additional details

## Supported tags and respective `Dockerfile` links

### Simple Tags
* **master** - the latest image build from the master branch of the project

## How to use this image

### Environment Variables
* List of the most important environment variables (all another environment variables could be created according to properties described in application [README](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker) and [Spring Externalized Configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config):
* `TRACKED_DATABASE_JDBC_URL`, `TRACKED_DATABASE_USERNAME`, `TRACKED_DATABASE_PASSWORD` - **tracked database** jdbc connection properties
* `SETTINGS_DATABASE_JDBC_URL`, `SETTINGS_DATABASE_USERNAME`, `SETTINGS_DATABASE_PASSWORD` - **settings database** jdbc connection properties
* `TARGET_STORAGE_JDBC_URL`, `TARGET_STORAGE_USERNAME`, `TARGET_STORAGE_PASSWORD` - **target storage** jdbc connection properties
* `TARGET_STORAGE_TYPE` - type of the **target storage** (one of the supported storages from the list above)

### Volumes
* `/config/application.properties` - path to external properties file (can be used to override application properties described in [README](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker))

### Run SQL Server Change Tracker

```shell
docker container run --name sqlserver-change-tracker \
  -p 8080:8080 -p 9000:9000 -d \
  -e "SPRING_BOOT_ADMIN_CLIENT_URL=http://spring-boot-admin:9001" \
  -e "SETTINGS_DATABASE_JDBC_URL=jdbc:postgresql://settings-database:5432/change_tracker" \
  -e "SETTINGS_DATABASE_USERNAME=postgres" -e "SETTINGS_DATABASE_PASSWORD=password" \
  -e "TARGET_STORAGE_JDBC_URL=jdbc:sqlserver://target-storage:1433;database=change_tracker_target;" \
  -e "TARGET_STORAGE_USERNAME=sa" -e "TARGET_STORAGE_PASSWORD=password" -e "TARGET_STORAGE_TYPE=SQLServer" \
  -e "TRACKED_DATABASE_JDBC_URL=jdbc:sqlserver://tracked-storage:1433;database=change_tracker_source;" \
  -e "TRACKED_DATABASE_USERNAME=sa" -e "TRACKED_DATABASE_PASSWORD=password" \
  olexandrkucher/sqlserver-change-tracker:master
```

## Authors
* [**Oleksandr Kucher**](https://gitlab.com/olexandr.kucher) - _Initial development_

## License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/olexandr.kucher/sqlserver-change-tracker/-/blob/master/LICENSE) file for details.