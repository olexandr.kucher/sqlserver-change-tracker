package io.kol.sqlserver.changetracker.concurrent;

import io.kol.sqlserver.changetracker.sync.ChangeTrackingSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Service
@ConcurrentImpl
class ChangeTrackingSyncServiceConcurrentImpl implements ChangeTrackingSyncService {
    private final Logger logger = LoggerFactory.getLogger(ChangeTrackingSyncServiceConcurrentImpl.class);

    private final ReentrantLock lock;
    private final Duration tryLockTimeout;
    private final ChangeTrackingSyncService changeTrackingSyncService;

    ChangeTrackingSyncServiceConcurrentImpl(@Value("${sync.data.concurrent.try-lock-timeout}") Duration tryLockTimeout,
                                            @BaseImpl ChangeTrackingSyncService changeTrackingSyncService) {
        this.lock = new ReentrantLock();
        this.tryLockTimeout = tryLockTimeout;
        this.changeTrackingSyncService = changeTrackingSyncService;
    }

    @Override
    public void init(boolean dropAllExistingTables) {
        invokeWithLock(() -> changeTrackingSyncService.init(dropAllExistingTables));
    }

    @Override
    public void sync(boolean dropMissedTables) {
        invokeWithLock(() -> changeTrackingSyncService.sync(dropMissedTables));
    }

    @Override
    public void sync() {
        invokeWithLock(changeTrackingSyncService::sync);
    }

    private void invokeWithLock(Runnable runnable) {
        boolean isLockAcquired = false;
        try {
            isLockAcquired = lock.tryLock(tryLockTimeout.toMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            logger.error("TryLock operation completed with unexpected exception.", ex);
        }

        if(isLockAcquired) {
            try {
                runnable.run();
            } finally {
                lock.unlock();
            }
        } else {
            logger.warn("Lock wasn't acquired by current thread (timeout={}).", tryLockTimeout);
        }
    }
}
