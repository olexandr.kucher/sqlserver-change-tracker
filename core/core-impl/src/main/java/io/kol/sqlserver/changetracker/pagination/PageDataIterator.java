package io.kol.sqlserver.changetracker.pagination;

import io.kol.sqlserver.changetracker.model.PageRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

class PageDataIterator<T> implements Iterator<List<T>> {
    private final PageSupplier<T> pageSupplier;
    private final int itemsPerPage;

    private List<T> pageData;
    private boolean hasNext;
    private int pageNumber;

    PageDataIterator(PageSupplier<T> pageSupplier, int itemsPerPage) {
        this.pageSupplier = pageSupplier;
        this.itemsPerPage = itemsPerPage;
        this.pageData = new ArrayList<>();
        this.hasNext = true;
        this.pageNumber = 1;
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public List<T> next() {
        if (!hasNext()) {
            throw new NoSuchElementException("There are no available data.");
        }

        pageData = pageSupplier.getPage(new PageRequest(pageNumber, itemsPerPage));
        hasNext = pageData.size() == itemsPerPage;
        pageNumber++;
        return pageData;
    }
}