package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.SettingsService;
import io.kol.sqlserver.changetracker.TargetStorageService;
import io.kol.sqlserver.changetracker.TrackedDatabaseService;
import io.kol.sqlserver.changetracker.concurrent.BaseImpl;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import io.kol.sqlserver.changetracker.syncstrategy.SyncDataStrategy;
import io.kol.sqlserver.changetracker.syncstrategy.SyncStrategyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@BaseImpl
class ChangeTrackingSyncServiceImpl implements ChangeTrackingSyncService {
    private final Logger logger = LoggerFactory.getLogger(ChangeTrackingSyncServiceImpl.class);

    private final SettingsService settingsService;
    private final SyncDataProperties syncDataProperties;
    private final SyncStrategyFactory syncStrategyFactory;
    private final TargetStorageService targetStorageService;
    private final TrackedDatabaseService trackedDatabaseService;
    private final ParallelDataSyncService parallelDataSyncService;

    ChangeTrackingSyncServiceImpl(SettingsService settingsService,
                                  SyncDataProperties syncDataProperties,
                                  SyncStrategyFactory syncStrategyFactory,
                                  TargetStorageService targetStorageService,
                                  TrackedDatabaseService trackedDatabaseService,
                                  ParallelDataSyncService parallelDataSyncService) {
        this.settingsService = settingsService;
        this.syncDataProperties = syncDataProperties;
        this.syncStrategyFactory = syncStrategyFactory;
        this.targetStorageService = targetStorageService;
        this.trackedDatabaseService = trackedDatabaseService;
        this.parallelDataSyncService = parallelDataSyncService;
    }

    @Override
    public void init(boolean dropAllExistingTables) {
        logger.info("Start init target storage structure...");
        final List<Table> tables = trackedDatabaseService.getAllTrackedTables();
        logger.info("Saving source storage structure into settings storage: {}", tables);
        settingsService.saveTrackedTables(tables);
        logger.info("Removing last sync data from settings storage...");
        settingsService.deleteLastSyncData();
        logger.info("Initializing target storage structure using tables={}, dropAllExistingTables={}...", tables, dropAllExistingTables);
        targetStorageService.initStorageStructure(tables, dropAllExistingTables);
        logger.info("Target storage structure initialized successfully.");
    }

    @Override
    public void sync(boolean dropMissedTables) {
        logger.info("Start sync structure and data between source and target storages...");
        syncStructure(dropMissedTables);
        syncData();
        logger.info("Sync between source and target storages completed successfully.");
    }

    @Override
    public void sync() {
        logger.info("Start sync structure and data between source and target storages...");
        syncStructure(syncDataProperties.isDropMissedTablesOnSync());
        syncData();
        logger.info("Sync between source and target storages completed successfully.");
    }

    private void syncStructure(boolean dropMissedTables) {
        logger.debug("Start alter target storage structure...");
        final List<Table> tables = trackedDatabaseService.getAllTrackedTables();
        logger.debug("Saving source storage structure into settings storage: {}", tables);
        settingsService.saveTrackedTables(tables);
        logger.debug("Altering target storage using tables={}...", tables);

        logger.info("Start modifying target storage structure using tables={}, dropMissedTables={}...", tables, dropMissedTables);
        targetStorageService.alterStorageStructure(tables, dropMissedTables);
        logger.info("Target storage structure modified successfully.");
    }

    private void syncData() {
        final List<Table> tables = settingsService.getAllTrackedTables();
        if (syncDataProperties.isUseParallelDataTransfer()) {
            parallelDataSyncService.syncData(this::syncSingleTableData, tables);
        } else {
            tables.forEach(this::syncSingleTableData);
        }
    }

    private void syncSingleTableData(Table table) {
        try {
            logger.debug("Sync data for table={}...", table);
            final SyncDataStrategy strategy = syncStrategyFactory.determineSyncStrategy(table);
            final long syncVersion = strategy.sync();
            settingsService.saveLastSyncVersion(table, syncVersion);
            logger.debug("Sync for table={} completed up to version={}.", table, syncVersion);
        } catch (Exception ex) {
            logger.warn("Unexpected exception while sync data for table={}, mark for full re-init.", table, ex);
            settingsService.markForFullReInitIfRequired(table);
        }
    }
}
