package io.kol.sqlserver.changetracker.metric;

import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TableRow;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

@Aspect
@Component
public class StorageDataMetricAspect {
    private final MeterRegistry meterRegistry;

    public StorageDataMetricAspect(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.sync.StorageDataService.cleanExistingTable(..))")
    public void cleanExistingTable() {
        //StorageDataService#cleanExistingTable Prometheus metrics pointcut
    }

    @Around(value = "cleanExistingTable() && args(table)", argNames = "pjp,table")
    public Object cleanExistingTableTimer(ProceedingJoinPoint pjp, Table table) throws Throwable {
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createCleanExistingTableTimer(table));
        }
    }

    private Timer createCleanExistingTableTimer(Table table) {
        final Timer.Builder builder = Timer.builder("change.tracker.clean.table.timer")
                .maximumExpectedValue(Duration.ofMinutes(1))
                .publishPercentileHistogram()
                .tag("schema", table.getTableSchema())
                .tag("table", table.getTableName());
        return builder.register(meterRegistry);
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.sync.StorageDataService.applyDataChanges(..))")
    public void applyDataChanges() {
        //StorageDataService#applyDataChanges Prometheus metrics pointcut
    }

    @Around(value = "applyDataChanges() && args(table,values)", argNames = "pjp,table,values")
    public Object applyDataChangesTimer(ProceedingJoinPoint pjp, Table table, List<TableRow> values) throws Throwable { //
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createApplyDataChangesTimer(table, values.size()));
        }
    }

    private Timer createApplyDataChangesTimer(Table table, int valuesCount) {
        final Timer.Builder builder = Timer.builder("change.tracker.apply.data.changes.timer")
                .maximumExpectedValue(Duration.ofHours(1))
                .publishPercentileHistogram()
                .tag("schema", table.getTableSchema())
                .tag("table", table.getTableName())
                .tag("valuesCount", String.valueOf(valuesCount));
        return builder.register(meterRegistry);
    }
}
