package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.SettingsService;
import io.kol.sqlserver.changetracker.exception.IncompatibleColumnChangeException;
import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;
import io.kol.sqlserver.changetracker.sql.DdlRepository;
import io.kol.sqlserver.changetracker.sql.SqlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@DependsOn("targetStorageTransactionalManager")
class StorageStructureServiceImpl implements StorageStructureService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final SqlMapper sqlMapper;
    private final Set<String> defaultSchemas;
    private final DdlRepository ddlRepository;
    private final SettingsService settingsService;
    private final ColumnEqualComparator columnEqualComparator;

    StorageStructureServiceImpl(SqlMapper sqlMapper,
                                DdlRepository ddlRepository,
                                SettingsService settingsService,
                                ColumnEqualComparator columnEqualComparator,
                                @Value("${target.storage.default.schemas:}") Set<String> defaultSchemas) {
        this.sqlMapper = sqlMapper;
        this.defaultSchemas = defaultSchemas;
        this.ddlRepository = ddlRepository;
        this.settingsService = settingsService;
        this.columnEqualComparator = columnEqualComparator;
    }

    @Override
    @Transactional(transactionManager = "targetStorageTransactionalManager")
    public void initStructure(List<Table> tables, boolean dropAllExistingTables) {
        logger.debug("Start initialize target storage schema with tables{}, dropAllExistingTables={}...", tables, dropAllExistingTables);
        final Set<String> trackedSchemas = getTrackedSchemas(tables);
        if (dropAllExistingTables) {
            logger.debug("Dropping all existing tables in target storage using schemas={}...", trackedSchemas);
            dropAllExistingTables(trackedSchemas);
            logger.debug("Dropping all existing schemas={} in target storage...", trackedSchemas);
            dropAllExistingSchemas(trackedSchemas);
        } else {
            logger.debug("Dropping tables={} in target storage...", tables);
            dropExistingTables(tables);
        }

        logger.debug("Creating schemas={} in target storage...", trackedSchemas);
        createSchemasIfRequired(trackedSchemas);
        logger.debug("Creating tables={} in target storage...", tables);
        createTables(tables);
        logger.debug("Target storage initialized successfully.");
    }

    @Override
    @Transactional(transactionManager = "targetStorageTransactionalManager")
    public void modifyStructure(List<Table> tables, boolean dropMissedTables) {
        logger.debug("Start modifying target storage structure with tables={}, dropMissedTables={}...", tables, dropMissedTables);
        syncTables(tables, dropMissedTables);
        syncColumns(tables);
        logger.debug("Target storage structure modified successfully.");
    }

    private void dropAllExistingTables(Set<String> trackedSchemas) {
        final List<TargetTable> tablesToDrop = trackedSchemas
                .stream()
                .map(ddlRepository::getAllTablesInSchema)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        tablesToDrop.forEach(ddlRepository::dropTable);
    }

    private void dropAllExistingSchemas(Set<String> trackedSchemas) {
        final List<String> schemasToDrop = trackedSchemas
                .stream()
                .filter(Predicate.not(defaultSchemas::contains))
                .collect(Collectors.toList());
        schemasToDrop.forEach(ddlRepository::dropSchema);
    }

    private void dropExistingTables(List<Table> tables) {
        tables.stream()
                .map(table -> new TargetTable(table.getTableSchema(), table.getTableName()))
                .forEach(ddlRepository::dropTable);
    }

    private void createSchemasIfRequired(Set<String> trackedSchemas) {
        trackedSchemas.stream()
                .filter(Predicate.not(ddlRepository::isSchemaExists))
                .forEach(ddlRepository::createSchema);
    }

    private void createTables(List<Table> tables) {
        tables.forEach(ddlRepository::createTable);
    }

    private void syncTables(List<Table> tables, boolean dropMissedTables) {
        logger.debug("Sync tables={} in target storage...", tables);
        final Set<String> trackedSchemas = getTrackedSchemas(tables);
        final Set<TargetTable> targetTables = trackedSchemas
                .stream()
                .map(ddlRepository::getAllTablesInSchema)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        if (dropMissedTables) {
            logger.debug("Dropping missed/untracked tables in target storage...");
            dropUntrackedTables(tables, targetTables);
        }

        logger.debug("Creating schemas={} in target storage...", trackedSchemas);
        createSchemasIfRequired(trackedSchemas);
        logger.debug("Creating new tables in target storage...");
        createTablesIfRequired(tables, targetTables);
    }

    private void dropUntrackedTables(List<Table> tables, Set<TargetTable> targetTables) {
        final Set<TargetTable> sourceTables = tables.stream()
                .map(table -> new TargetTable(
                        sqlMapper.remapSqlName(table.getTableSchema()),
                        sqlMapper.remapSqlName(table.getTableName())
                )).collect(Collectors.toSet());
        for (TargetTable targetTable : targetTables) {
            if (!sourceTables.contains(targetTable)) {
                ddlRepository.dropTable(targetTable);
            }
        }
    }

    private void createTablesIfRequired(List<Table> tables, Set<TargetTable> targetTables) {
        for (Table table : tables) {
            final TargetTable sourceTable = new TargetTable(
                    sqlMapper.remapSqlName(table.getTableSchema()),
                    sqlMapper.remapSqlName(table.getTableName())
            );
            if (!targetTables.contains(sourceTable)) {
                ddlRepository.createTable(table);
            }
        }
    }

    private void syncColumns(List<Table> tables) {
        final Map<TargetTable, List<Column>> tableColumns = ddlRepository.getAllTableColumns();
        for (Table table : tables) {
            try {
                logger.debug("Sync columns in target storage for table={}...", table);
                final TargetTable sourceTable = new TargetTable(
                        sqlMapper.remapSqlName(table.getTableSchema()),
                        sqlMapper.remapSqlName(table.getTableName())
                );
                syncColumns(table, tableColumns.get(sourceTable));
            } catch (IncompatibleColumnChangeException ex) {
                logger.warn("Cannot alter target storage for table={}, recreating table...", table, ex);
                initStructure(List.of(table), false);
                logger.info("Table={} successfully recreated in target storage, mark for full re-init.", table);
                settingsService.markForFullReInit(table);
            }
        }
    }

    private void syncColumns(Table table, List<Column> columns) {
        logger.debug("Dropping untracked columns from target storage table={}...", table);
        dropUntrackedColumns(table, columns);
        logger.debug("Creating new columns in target storage table={}...", table);
        createNewColumns(table, columns);
        logger.debug("Altering modified columns in target storage table={}...", table);
        alterChangedColumns(table, columns);
        logger.debug("Sync columns in target storage for table={} completed successfully.", table);
    }

    private void dropUntrackedColumns(Table table, List<Column> targetColumns) {
        try {
            final Set<String> sourceColumnNames = table.getColumns()
                    .stream()
                    .map(Column::getColumnName)
                    .map(sqlMapper::remapSqlName)
                    .collect(Collectors.toSet());
            for (Column targetColumn : targetColumns) {
                final String targetColumnName = sqlMapper.remapSqlName(targetColumn.getColumnName());
                if (!sourceColumnNames.contains(targetColumnName)) {
                    ddlRepository.dropColumn(table, targetColumn);
                }
            }
        } catch (Exception ex) {
            throw new IncompatibleColumnChangeException(ex);
        }
    }

    private void createNewColumns(Table table, List<Column> targetColumns) {
        try {
            final Set<String> targetColumnNames = targetColumns
                    .stream()
                    .map(Column::getColumnName)
                    .map(sqlMapper::remapSqlName)
                    .collect(Collectors.toSet());
            for (Column sourceColumn : table.getColumns()) {
                final String sourceColumnName = sqlMapper.remapSqlName(sourceColumn.getColumnName());
                if (!targetColumnNames.contains(sourceColumnName)) {
                    ddlRepository.createColumn(table, sourceColumn);
                }
            }
        } catch (Exception ex) {
            throw new IncompatibleColumnChangeException(ex);
        }
    }

    private void alterChangedColumns(Table table, List<Column> targetColumns) {
        try {
            final Map<String, Column> targetColumnNames = targetColumns
                    .stream()
                    .collect(Collectors.toMap(col -> sqlMapper.remapSqlName(col.getColumnName()), Function.identity()));
            for (Column sourceColumn : table.getColumns()) {
                final Column targetColumn = targetColumnNames.get(sqlMapper.remapSqlName(sourceColumn.getColumnName()));
                if (Objects.nonNull(targetColumn) && !columnEqualComparator.isEquals(sourceColumn, targetColumn)) {
                    ddlRepository.alterColumn(table, sourceColumn, targetColumn);
                }
            }
        } catch (Exception ex) {
            throw new IncompatibleColumnChangeException(ex);
        }
    }

    private Set<String> getTrackedSchemas(List<Table> tables) {
        return tables.stream()
                .map(Table::getTableSchema)
                .collect(Collectors.toSet());
    }
}
