package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.distributedtracing.ExecutorServiceFactory;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

@Service
class ParallelDataSyncService {
    private final Logger logger = LoggerFactory.getLogger(ParallelDataSyncService.class);

    private final ExecutorService executorService;

    ParallelDataSyncService(SyncDataProperties syncDataProperties,
                            ExecutorServiceFactory executorServiceFactory) {
        executorService = executorServiceFactory.newFixedThreadPool(
                syncDataProperties.getParallelThreadsCount(),
                "sqlserver-change-tracker-parallel-data-sync",
                "parallel-data-sync"
        );
    }

    void syncData(Consumer<Table> syncSingleTableData, List<Table> tables) {
        try {
            CompletableFuture.allOf(tables.stream()
                    .map(table -> CompletableFuture.runAsync(
                            () -> syncSingleTableData.accept(table),
                            executorService
                    )).toArray(CompletableFuture[]::new)
            ).get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Unexpected exception while run parallel data sync.", e);
        } catch (ExecutionException e) {
            logger.error("Unexpected exception while run parallel data sync.", e);
        }
    }

    @PreDestroy
    void shutdown() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(30, TimeUnit.SECONDS)) {
                logger.warn("Threads didn't finish in 30 seconds");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Unexpected exception while shutdown ParallelTaskExecutor.", e);
        }
    }
}
