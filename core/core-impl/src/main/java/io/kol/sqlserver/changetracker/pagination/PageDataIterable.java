package io.kol.sqlserver.changetracker.pagination;

import java.util.Iterator;
import java.util.List;

class PageDataIterable<T> implements DataIterable<T> {
    private final PageSupplier<T> pageSupplier;
    private final CountSupplier countSupplier;
    private final int itemsPerPage;

    PageDataIterable(PageSupplier<T> pageSupplier, CountSupplier countSupplier, int itemsPerPage) {
        this.pageSupplier = pageSupplier;
        this.countSupplier = countSupplier;
        this.itemsPerPage = itemsPerPage;
    }

    @Override
    public Iterator<List<T>> iterator() {
        return new PageDataIterator<>(pageSupplier, itemsPerPage);
    }

    @Override
    public long countAll() {
        return countSupplier.getCount();
    }
}
