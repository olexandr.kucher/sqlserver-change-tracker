package io.kol.sqlserver.changetracker.pagination;

import io.kol.sqlserver.changetracker.distributedtracing.ExecutorServiceFactory;
import io.kol.sqlserver.changetracker.model.SyncDataType;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import org.springframework.stereotype.Component;

import static io.kol.sqlserver.changetracker.model.SyncDataType.DELTA;

@Component
class DataIterableFactoryImpl implements DataIterableFactory {
    private final boolean useBufferedDataRead;
    private final ExecutorServiceFactory executorServiceFactory;

    DataIterableFactoryImpl(SyncDataProperties syncDataProperties,
                            ExecutorServiceFactory executorServiceFactory) {
        this.useBufferedDataRead = syncDataProperties.isUseBufferedDataReader();
        this.executorServiceFactory = executorServiceFactory;
    }

    @Override
    public <T> DataIterable<T> createDataIterable(
            SyncDataType syncDataType,
            PageSupplier<T> pageSupplier,
            CountSupplier countSupplier,
            int itemsPerPage,
            Table table
    ) {
        if (syncDataType == DELTA) {
            return new PageDataIterable<>(pageSupplier, countSupplier, itemsPerPage);
        }

        if (useBufferedDataRead) {
            return new PageDataBufferedIterable<>(
                    executorServiceFactory, pageSupplier,
                    countSupplier, itemsPerPage, table
            );
        } else {
            return new PageDataIterable<>(pageSupplier, countSupplier, itemsPerPage);
        }
    }
}
