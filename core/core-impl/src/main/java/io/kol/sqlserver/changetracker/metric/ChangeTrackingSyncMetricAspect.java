package io.kol.sqlserver.changetracker.metric;

import io.kol.sqlserver.changetracker.SettingsService;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.concurrent.atomic.AtomicLong;

import static io.kol.sqlserver.changetracker.model.MetadataType.INIT_EPOCH_SECONDS;
import static io.kol.sqlserver.changetracker.model.MetadataType.SYNC_EPOCH_SECONDS;

@Aspect
@Component
public class ChangeTrackingSyncMetricAspect {
    private final MeterRegistry meterRegistry;
    private final SettingsService settingsService;
    private final SyncDataProperties syncDataProperties;

    private final AtomicLong lastInitStructureTime;
    private final AtomicLong lastSyncTime;

    public ChangeTrackingSyncMetricAspect(MeterRegistry meterRegistry,
                                          SettingsService settingsService,
                                          SyncDataProperties syncDataProperties) {
        this.meterRegistry = meterRegistry;
        this.settingsService = settingsService;
        this.syncDataProperties = syncDataProperties;
        this.lastInitStructureTime = new AtomicLong(INIT_EPOCH_SECONDS.metadata(settingsService));
        this.lastSyncTime = new AtomicLong(SYNC_EPOCH_SECONDS.metadata(settingsService));

        Gauge.builder("change.tracker.init.structure.timestamp", lastInitStructureTime::longValue)
                .description("Time of init target storage since unix epoch in seconds.")
                .register(meterRegistry);
        Gauge.builder("change.tracker.last.sync.timestamp", lastSyncTime::longValue)
                .description("Last time of sync data and structure from tracked database into target storage since unix epoch in seconds.")
                .register(meterRegistry);
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.sync.ChangeTrackingSyncService.init(..))")
    public void initStructure() {
        //ChangeTrackingSyncService#init Prometheus metrics pointcut
    }

    @Around(value = "initStructure() && args(dropAllExistingTables)", argNames = "pjp,dropAllExistingTables")
    public Object initStructureTimer(ProceedingJoinPoint pjp, boolean dropAllExistingTables) throws Throwable {
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createInitStructureTimer(dropAllExistingTables));
        }
    }

    @After(value = "initStructure()")
    public void initStructureTime() {
        final long epochSecond = OffsetDateTime.now().toEpochSecond();
        lastInitStructureTime.set(epochSecond);
        settingsService.saveMetadata(INIT_EPOCH_SECONDS, epochSecond);
    }

    private Timer createInitStructureTimer(boolean dropAllExistingTables) {
        final Timer.Builder builder = Timer.builder("change.tracker.init.structure.timer")
                .maximumExpectedValue(Duration.ofMinutes(10))
                .publishPercentileHistogram()
                .tag("dropAllExistingTables", String.valueOf(dropAllExistingTables));
        return builder.register(meterRegistry);
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.sync.ChangeTrackingSyncService.sync(..))")
    public void sync() {
        //ChangeTrackingSyncService#sync Prometheus metrics pointcut
    }

    @Around(value = "sync() && args(dropMissedTables)", argNames = "pjp,dropMissedTables")
    public Object syncTimer(ProceedingJoinPoint pjp, boolean dropMissedTables) throws Throwable {
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createSyncTimer(dropMissedTables));
        }
    }

    @Around(value = "sync() && args()", argNames = "pjp")
    public Object syncTimer(ProceedingJoinPoint pjp) throws Throwable {
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createSyncTimer(syncDataProperties.isDropMissedTablesOnSync()));
        }
    }

    @After(value = "sync()")
    public void syncTime() {
        final long epochSecond = OffsetDateTime.now().toEpochSecond();
        lastSyncTime.set(epochSecond);
        settingsService.saveMetadata(SYNC_EPOCH_SECONDS, epochSecond);
    }

    private Timer createSyncTimer(boolean dropMissedTables) {
        final Timer.Builder builder = Timer.builder("change.tracker.full.sync.timer")
                .maximumExpectedValue(Duration.ofHours(1))
                .publishPercentileHistogram()
                .tag("dropMissedTables", String.valueOf(dropMissedTables));
        return builder.register(meterRegistry);
    }
}
