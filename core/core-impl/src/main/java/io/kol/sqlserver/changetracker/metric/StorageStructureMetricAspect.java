package io.kol.sqlserver.changetracker.metric;

import io.kol.sqlserver.changetracker.model.Table;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

@Aspect
@Component
public class StorageStructureMetricAspect {
    private final MeterRegistry meterRegistry;

    public StorageStructureMetricAspect(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.sync.StorageStructureService.modifyStructure(..))")
    public void modifyStructure() {
        //StorageStructureService#modifyStructure Prometheus metrics pointcut
    }

    @Around(value = "modifyStructure() && args(tables,dropMissedTables)", argNames = "pjp,tables,dropMissedTables")
    public Object modifyStructureTimer(ProceedingJoinPoint pjp, List<Table> tables, boolean dropMissedTables) throws Throwable {
        final Timer.Sample timerSample = Timer.start(meterRegistry);
        try {
            return pjp.proceed();
        } finally {
            timerSample.stop(createModifyStructureTimer(tables.size(), dropMissedTables));
        }
    }

    private Timer createModifyStructureTimer(int tablesCount, boolean dropMissedTables) {
        final Timer.Builder builder = Timer.builder("change.tracker.modify.structure.timer")
                .maximumExpectedValue(Duration.ofMinutes(10))
                .publishPercentileHistogram()
                .tag("tablesCount", String.valueOf(tablesCount))
                .tag("dropMissedTables", String.valueOf(dropMissedTables));
        return builder.register(meterRegistry);
    }
}
