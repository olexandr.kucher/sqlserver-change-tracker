package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.DataChanges;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.sync.StorageDataService;
import io.kol.sqlserver.changetracker.sync.StorageStructureService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class TargetStorageServiceImpl implements TargetStorageService {
    private final StorageStructureService storageStructureService;
    private final StorageDataService storageDataService;

    TargetStorageServiceImpl(StorageStructureService storageStructureService,
                             StorageDataService storageDataService) {
        this.storageStructureService = storageStructureService;
        this.storageDataService = storageDataService;
    }

    @Override
    public void initStorageStructure(List<Table> tables, boolean dropAllExistingTables) {
        storageStructureService.initStructure(tables, dropAllExistingTables);
    }

    @Override
    public void alterStorageStructure(List<Table> tables, boolean dropMissedTables) {
        storageStructureService.modifyStructure(tables, dropMissedTables);
    }

    @Override
    public void cleanExistingTable(Table table) {
        storageDataService.cleanExistingTable(table);
    }

    @Override
    public void applyDmlChanges(Table table, DataChanges dataChanges) {
        storageDataService.applyDataChanges(table, dataChanges);
    }
}
