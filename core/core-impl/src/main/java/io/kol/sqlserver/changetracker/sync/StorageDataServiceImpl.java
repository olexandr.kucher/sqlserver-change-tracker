package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.model.*;
import io.kol.sqlserver.changetracker.sql.DmlRepository;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.kol.sqlserver.changetracker.model.SyncDataType.FULL;

@Service
@DependsOn("targetStorageTransactionalManager")
class StorageDataServiceImpl implements StorageDataService {
    private final DmlRepository dmlRepository;

    StorageDataServiceImpl(DmlRepository dmlRepository) {
        this.dmlRepository = dmlRepository;
    }

    @Override
    public void cleanExistingTable(Table table) {
        dmlRepository.truncate(table);
    }

    @Override
    @Transactional(transactionManager = "targetStorageTransactionalManager")
    public void applyDataChanges(Table table, DataChanges dataChanges) {
        if (dataChanges.values().isEmpty()) {
            return;
        }

        final Map<ChangeOperation, List<TableRow>> dataToApply = dataChanges.values().stream()
                .collect(Collectors.groupingBy(TableRow::getOperation));

        // 1. issues with composite primary key can appear here (in one of repository methods)
        //     in case composite primary key changed (for example when one of columns included into primary key updated)
        // 2. let's use regular INSERT for full re-import (to avoid performance degradation)
        //    and MERGE for delta sync (to avoid primary key constraint violation if row with
        //    such key was already inserted by previous delta sync that inserted few batches and then failed)
        if (dataChanges.syncDataType() == FULL) {
            dmlRepository.create(table, extractData(dataToApply, ChangeOperation.CREATE, TableRow::getRow));
        } else {
            dmlRepository.merge(table, extractData(dataToApply, ChangeOperation.CREATE, TableRow::getRow));
        }
        dmlRepository.update(table, extractData(dataToApply, ChangeOperation.UPDATE, TableRow::getRow));
        dmlRepository.delete(table, extractData(dataToApply, ChangeOperation.DELETE, TableRow::getPrimaryKey));
    }

    private <T> List<T> extractData(Map<ChangeOperation, List<TableRow>> dataToApply,
                                    ChangeOperation changeOperation,
                                    Function<TableRow, T> mapper) {
        return dataToApply.getOrDefault(changeOperation, List.of())
                .stream()
                .map(mapper)
                .collect(Collectors.toList());
    }
}
