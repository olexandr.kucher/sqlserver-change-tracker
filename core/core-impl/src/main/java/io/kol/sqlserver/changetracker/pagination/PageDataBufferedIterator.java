package io.kol.sqlserver.changetracker.pagination;

import com.google.common.base.Stopwatch;
import io.kol.sqlserver.changetracker.distributedtracing.ExecutorServiceFactory;
import io.kol.sqlserver.changetracker.exception.PageDataReadException;
import io.kol.sqlserver.changetracker.model.PageRequest;
import io.kol.sqlserver.changetracker.model.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

class PageDataBufferedIterator<T> implements Iterator<List<T>> {
    private static final int DATA_MAX_WAIT_TIME_SEC = 200;
    private static final int BUFFER_SIZE = 4;

    private final Logger logger = LoggerFactory.getLogger(PageDataBufferedIterator.class);

    private final PageSupplier<T> pageSupplier;
    private final int itemsPerPage;
    private final Table table;

    private final ArrayBlockingQueue<List<T>> indexDataBuffer;
    private final ExecutorService executorService;
    private volatile boolean nextPageExists;

    PageDataBufferedIterator(
            ExecutorServiceFactory executorServiceFactory,
            PageSupplier<T> pageSupplier,
            int itemsPerPage,
            Table table
    ) {
        this.pageSupplier = pageSupplier;
        this.itemsPerPage = itemsPerPage;
        this.table = table;

        this.nextPageExists = true;
        this.indexDataBuffer = new ArrayBlockingQueue<>(BUFFER_SIZE, true);
        this.executorService = executorServiceFactory.newSingleThreadExecutor(
                "sqlserver-change-tracker-iterator-"
                        + table.getTableSchema().toLowerCase() + "-"
                        + table.getTableName().toLowerCase(),
                "iterator-"
                        + table.getTableSchema().toLowerCase() + "-"
                        + table.getTableName().toLowerCase()
        );

        executorService.submit(this::safeReadPageData);
        executorService.shutdown();
    }

    @Override
    public boolean hasNext() {
        return nextPageExists || !indexDataBuffer.isEmpty();
    }

    @Override
    public List<T> next() {
        try {
            List<T> data = indexDataBuffer.poll(DATA_MAX_WAIT_TIME_SEC, TimeUnit.SECONDS);
            if (Objects.isNull(data)) {
                safeCloseIterator();
                throw new NoSuchElementException(
                        "Waiting time elapsed (" + DATA_MAX_WAIT_TIME_SEC + " sec) "
                                + "before an data from " + table + " is available."
                );
            }
            return data;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            safeCloseIterator();
            throw new PageDataReadException("Reading data from " + table + " was interrupted.", e);
        }
    }

    private void safeReadPageData() {
        try {
            readPageData();
        } catch (Exception e) {
            logger.error("Exception happened while reading index data.", e);
        }
    }

    private void readPageData() {
        final Stopwatch fullReadStopwatch = Stopwatch.createStarted();
        logger.info("Start reading data from table={}...", table);
        int currentPage = 1;
        do {
            final Stopwatch stopwatch = Stopwatch.createStarted();
            final List<T> pageData = pageSupplier.getPage(new PageRequest(currentPage, itemsPerPage));

            if (pageData.isEmpty()) {
                nextPageExists = false;
            } else {
                logger.info(
                        "{} data rows from table={} were read, duration={}.",
                        itemsPerPage * (currentPage - 1) + pageData.size(),
                        table, stopwatch
                );
                currentPage++;
            }
            putIntoBuffer(pageData);
        } while (nextPageExists);
        logger.info("Reading data from table={} completed successfully, duration={}.", table, fullReadStopwatch);
    }

    private void putIntoBuffer(List<T> pageData) {
        try {
            indexDataBuffer.put(pageData);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new PageDataReadException("Adding data to the buffer read from " + table + " was interrupted.", e);
        }
    }

    private void safeCloseIterator() {
        logger.warn("Closing buffered iterator for table={} because of unexpected error.", table);
        nextPageExists = false;
        try {
            if (!executorService.awaitTermination(100, TimeUnit.SECONDS)) {
                logger.warn("Threads didn't finish in 100 seconds");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Unexpected exception while shutdown internal executor of PageDataBufferedIterator.", e);
        }
    }
}
