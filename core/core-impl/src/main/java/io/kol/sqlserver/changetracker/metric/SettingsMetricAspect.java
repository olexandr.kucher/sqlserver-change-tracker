package io.kol.sqlserver.changetracker.metric;

import io.kol.sqlserver.changetracker.SettingsService;
import io.kol.sqlserver.changetracker.model.Table;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static io.kol.sqlserver.changetracker.model.MetadataType.TRACKED_TABLES_COUNT;

@Aspect
@Component
public class SettingsMetricAspect {
    private final SettingsService settingsService;
    private final AtomicInteger trackedTablesCount;

    public SettingsMetricAspect(MeterRegistry meterRegistry, SettingsService settingsService) {
        this.settingsService = settingsService;
        this.trackedTablesCount = new AtomicInteger(TRACKED_TABLES_COUNT.metadata(settingsService));

        Gauge.builder("change.tracker.tracked.tables.count", trackedTablesCount::longValue)
                .description("Number of tables tracked by this application")
                .register(meterRegistry);
    }

    @Pointcut("execution(* io.kol.sqlserver.changetracker.SettingsService.saveTrackedTables(..))")
    public void saveTrackedTables() {
        //SettingsService#saveTrackedTables Prometheus metrics pointcut
    }

    @After(value = "saveTrackedTables() && args(tables)", argNames = "tables")
    public void saveTrackedTablesCounter(List<Table> tables) {
        trackedTablesCount.set(tables.size());
        settingsService.saveMetadata(TRACKED_TABLES_COUNT, tables.size());
    }
}
