package io.kol.sqlserver.changetracker.pagination;

import io.kol.sqlserver.changetracker.distributedtracing.ExecutorServiceFactory;
import io.kol.sqlserver.changetracker.model.Table;

import java.util.Iterator;
import java.util.List;

class PageDataBufferedIterable<T> implements DataIterable<T> {
    private final ExecutorServiceFactory executorServiceFactory;
    private final PageSupplier<T> pageSupplier;
    private final CountSupplier countSupplier;
    private final int itemsPerPage;
    private final Table table;

    PageDataBufferedIterable(
            ExecutorServiceFactory executorServiceFactory,
            PageSupplier<T> pageSupplier,
            CountSupplier countSupplier,
            int itemsPerPage,
            Table table
    ) {
        this.executorServiceFactory = executorServiceFactory;
        this.pageSupplier = pageSupplier;
        this.countSupplier = countSupplier;
        this.itemsPerPage = itemsPerPage;
        this.table = table;
    }

    @Override
    public Iterator<List<T>> iterator() {
        return new PageDataBufferedIterator<>(executorServiceFactory, pageSupplier, itemsPerPage, table);
    }

    @Override
    public long countAll() {
        return countSupplier.getCount();
    }
}
