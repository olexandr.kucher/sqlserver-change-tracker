package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.PrimaryKeyValue;
import io.kol.sqlserver.changetracker.model.Table;

import java.util.List;
import java.util.Map;

public interface DmlRepository {

    void truncate(Table table);

    void create(Table table, List<Map<String, Object>> data);

    void merge(Table table, List<Map<String, Object>> data);

    void update(Table table, List<Map<String, Object>> data);

    void delete(Table table, List<PrimaryKeyValue> primaryKeyValues);
}
