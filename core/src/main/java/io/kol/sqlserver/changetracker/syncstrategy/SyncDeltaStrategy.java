package io.kol.sqlserver.changetracker.syncstrategy;

import com.google.common.base.Stopwatch;
import io.kol.sqlserver.changetracker.TrackedDatabaseService;
import io.kol.sqlserver.changetracker.model.SyncTableData;
import io.kol.sqlserver.changetracker.model.TableRow;
import io.kol.sqlserver.changetracker.pagination.DataIterable;
import io.kol.sqlserver.changetracker.pagination.DataIterableFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.kol.sqlserver.changetracker.model.SyncDataType.DELTA;

class SyncDeltaStrategy implements SyncDataStrategy {
    private final Logger logger = LoggerFactory.getLogger(SyncDeltaStrategy.class);

    private final SyncTableData syncTableData;
    private final TransferDataService transferDataService;
    private final DataIterableFactory dataIterableFactory;
    private final TrackedDatabaseService trackedDatabaseService;

    SyncDeltaStrategy(SyncTableData syncTableData,
                      TransferDataService transferDataService,
                      DataIterableFactory dataIterableFactory,
                      TrackedDatabaseService trackedDatabaseService) {
        this.syncTableData = syncTableData;
        this.transferDataService = transferDataService;
        this.dataIterableFactory = dataIterableFactory;
        this.trackedDatabaseService = trackedDatabaseService;
    }

    @Override
    public long sync() {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        logger.info(
                "Start delta import of data for table={} ({}->{})...", syncTableData.getTable(),
                syncTableData.getLastSyncVersion(), syncTableData.getCurrentVersion()
        );

        final DataIterable<TableRow> iterable = dataIterableFactory.createDataIterable(DELTA, pageRequest -> trackedDatabaseService
                .getUntrackedTableValues(
                        syncTableData.getTable(),
                        syncTableData.getLastSyncVersion(),
                        syncTableData.getCurrentVersion(),
                        pageRequest
                ), () -> trackedDatabaseService.countAllUntrackedTableValues(
                        syncTableData.getTable(),
                        syncTableData.getLastSyncVersion(),
                        syncTableData.getCurrentVersion()
                ), syncTableData.getSyncBatchSize(),
                syncTableData.getTable()
        );

        transferDataService.transferData(iterable, syncTableData);

        logger.info(
                "Delta import of data for table={} ({}->{}) completed successfully (spent time={}).",
                syncTableData.getTable(), syncTableData.getLastSyncVersion(),
                syncTableData.getCurrentVersion(), stopwatch.stop()
        );
        return syncTableData.getCurrentVersion();
    }
}
