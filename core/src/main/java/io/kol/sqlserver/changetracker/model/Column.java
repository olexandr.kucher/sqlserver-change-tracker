package io.kol.sqlserver.changetracker.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.sql.Types;
import java.util.Objects;

@JsonDeserialize(builder = Column.Builder.class)
public class Column {
    private final int position;
    private final String columnName;
    private final String dataType;
    private final Integer maxLength;
    private final boolean nullable;
    private final String collationName;

    private Column(Builder builder) {
        this.position = builder.position;
        this.columnName = builder.columnName;
        this.dataType = builder.dataType;
        this.maxLength = builder.maxLength;
        this.nullable = builder.nullable;
        this.collationName = builder.collationName;
    }

    public int getPosition() {
        return position;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public boolean isNullable() {
        return nullable;
    }

    public String getCollationName() {
        return collationName;
    }

    public int getJavaSqlType() {
        return switch (dataType.toLowerCase()) {
            case "bit" -> Types.BIT;
            case "smallint" -> Types.SMALLINT;
            case "tinyint" -> Types.TINYINT;
            case "int" -> Types.INTEGER;
            case "bigint" -> Types.BIGINT;
            case "real" -> Types.REAL;
            case "float" -> Types.DOUBLE;
            case "numeric" -> Types.NUMERIC;
            case "decimal", "money", "smallmoney" -> Types.DECIMAL;
            case "nchar" -> Types.NCHAR;
            case "varchar" -> Types.VARCHAR;
            case "nvarchar" -> Types.NVARCHAR;
            case "text" -> Types.LONGVARCHAR;
            case "ntext", "xml" -> Types.LONGNVARCHAR;
            case "char", "uniqueidentifier" -> Types.CHAR;
            case "date" -> Types.DATE;
            case "time" -> Types.TIME;
            case "datetime", "datetime2", "smalldatetime" -> Types.TIMESTAMP;
            case "image" -> Types.LONGVARBINARY;
            case "binary", "timestamp", "rowversion" -> Types.BINARY;
            case "varbinary", "udt", "geometry", "geography" -> Types.VARBINARY;
            //case "datetimeoffset" -> microsoft.sql.Types.DATETIMEOFFSET; //microsoft.sql.DateTimeOffset
            //case "sqlvariant" -> microsoft.sql.Types.SQL_VARIANT; //Object
            default -> Integer.MIN_VALUE;
        };
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder {
        private int position;
        private String columnName;
        private String dataType;
        private Integer maxLength;
        private boolean nullable;
        private String collationName;

        private Builder() {
        }

        public Builder setPosition(int position) {
            this.position = position;
            return this;
        }

        public Builder setColumnName(String columnName) {
            this.columnName = columnName;
            return this;
        }

        public Builder setDataType(String dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder setMaxLength(Integer maxLength) {
            this.maxLength = maxLength;
            return this;
        }

        public Builder setNullable(boolean nullable) {
            this.nullable = nullable;
            return this;
        }

        public Builder setCollationName(String collationName) {
            this.collationName = collationName;
            return this;
        }

        public Column build() {
            return new Column(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Column that = (Column) obj;
        return position == that.position
                && nullable == that.nullable
                && Objects.equals(columnName, that.columnName)
                && Objects.equals(dataType, that.dataType)
                && Objects.equals(maxLength, that.maxLength)
                && Objects.equals(collationName, that.collationName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, columnName, dataType, maxLength, nullable, collationName);
    }
}
