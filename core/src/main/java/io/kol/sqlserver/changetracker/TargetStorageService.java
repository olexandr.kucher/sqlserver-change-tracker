package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.DataChanges;
import io.kol.sqlserver.changetracker.model.Table;

import java.util.List;

public interface TargetStorageService {
    void initStorageStructure(List<Table> tables, boolean dropAllExistingTables);

    void alterStorageStructure(List<Table> tables, boolean dropMissedTables);

    void cleanExistingTable(Table table);

    void applyDmlChanges(Table table, DataChanges dataChanges);
}
