package io.kol.sqlserver.changetracker.model;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PrimaryKey {
    private final List<Column> columns;

    public PrimaryKey(List<Column> columns) {
        this.columns = columns
                .stream()
                .sorted(Comparator.comparingInt(Column::getPosition))
                .collect(Collectors.toUnmodifiableList());
    }

    public List<Column> getColumns() {
        return columns;
    }

    public boolean notIncludes(String columnName) {
        return columns.stream().noneMatch(col -> col.getColumnName().equalsIgnoreCase(columnName));
    }

    public boolean notIncludes(Column column) {
        return !columns.contains(column);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PrimaryKey that = (PrimaryKey) obj;
        return Objects.equals(columns, that.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columns);
    }

    public static Predicate<Column> columnsToUpdate(PrimaryKey primaryKey, List<Column> columns) {
        if (Objects.equals(primaryKey.getColumns(), columns)) {
            //Let's update each field of the composite primary key
            //if update action got from SQL Server for many-to-many relation table.
            //Many-to-many relation table contains only 2 columns that are the primary key.
            //Other tables that have composite primary key built using all the columns of the table
            //will be processed in the same manner
            return columnName -> true;
        } else {
            return primaryKey::notIncludes;
        }
    }

    public static Predicate<String> columnNamesToUpdate(PrimaryKey primaryKey, List<Column> columns) {
        if (Objects.equals(primaryKey.getColumns(), columns)) {
            //Let's update each field of the composite primary key
            //if update action got from SQL Server for many-to-many relation table.
            //Many-to-many relation table contains only 2 columns that are the primary key.
            //Other tables that have composite primary key built using all the columns of the table
            //will be processed in the same manner
            return columnName -> true;
        } else {
            return primaryKey::notIncludes;
        }
    }
}
