package io.kol.sqlserver.changetracker.pagination;

import io.kol.sqlserver.changetracker.model.SyncDataType;
import io.kol.sqlserver.changetracker.model.Table;

public interface DataIterableFactory {
    <T> DataIterable<T> createDataIterable(
            SyncDataType syncDataType,
            PageSupplier<T> pageSupplier,
            CountSupplier countSupplier,
            int itemsPerPage,
            Table table
    );
}
