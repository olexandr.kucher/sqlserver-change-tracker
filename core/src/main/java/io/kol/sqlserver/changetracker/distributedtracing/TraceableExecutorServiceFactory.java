package io.kol.sqlserver.changetracker.distributedtracing;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.cloud.sleuth.instrument.async.TraceableExecutorService;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
class TraceableExecutorServiceFactory implements ExecutorServiceFactory {
    private final BeanFactory beanFactory;

    TraceableExecutorServiceFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public ExecutorService newSingleThreadExecutor(String threadNamePrefix, String spanName) {
        return new TraceableExecutorService(beanFactory, Executors.newSingleThreadExecutor(
                new CustomizableThreadFactory(String.format("%s-", threadNamePrefix))
        ), spanName);
    }

    @Override
    public ExecutorService newFixedThreadPool(int nThreads, String threadNamePrefix, String spanName) {
        return new TraceableExecutorService(beanFactory, Executors.newFixedThreadPool(
                nThreads,
                new CustomizableThreadFactory(String.format("%s-", threadNamePrefix))
        ), spanName);
    }
}
