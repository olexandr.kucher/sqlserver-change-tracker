package io.kol.sqlserver.changetracker.distributedtracing;

import java.util.concurrent.ExecutorService;

public interface ExecutorServiceFactory {
    ExecutorService newSingleThreadExecutor(String threadNamePrefix, String spanName);

    ExecutorService newFixedThreadPool(int nThreads, String threadNamePrefix, String spanName);
}
