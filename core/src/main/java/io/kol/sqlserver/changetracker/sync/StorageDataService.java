package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.model.DataChanges;
import io.kol.sqlserver.changetracker.model.Table;

public interface StorageDataService {

    void cleanExistingTable(Table table);

    void applyDataChanges(Table table, DataChanges dataChanges);
}
