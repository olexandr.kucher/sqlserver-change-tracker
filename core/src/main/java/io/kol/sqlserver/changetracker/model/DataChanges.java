package io.kol.sqlserver.changetracker.model;

import java.util.List;

public record DataChanges(List<TableRow> values, SyncDataType syncDataType) {
}
