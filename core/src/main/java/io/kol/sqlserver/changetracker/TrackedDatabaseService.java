package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.PageRequest;
import io.kol.sqlserver.changetracker.model.TableRow;
import io.kol.sqlserver.changetracker.model.Table;

import java.util.List;

public interface TrackedDatabaseService {
    long getCurrentVersion();

    long getMinimumValidVersion(Table table);

    List<Table> getAllTrackedTables();

    long countAllTableValues(Table table, long currentVersion);

    long countAllUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion);

    List<TableRow> getAllTableValues(Table table, long currentVersion, PageRequest pageRequest);

    List<TableRow> getUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion, PageRequest pageRequest);
}
