package io.kol.sqlserver.changetracker.syncstrategy;

import com.google.common.base.Stopwatch;
import io.kol.sqlserver.changetracker.TargetStorageService;
import io.kol.sqlserver.changetracker.model.DataChanges;
import io.kol.sqlserver.changetracker.model.SyncTableData;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TableRow;
import io.kol.sqlserver.changetracker.pagination.DataIterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class TransferDataService {
    private final Logger logger = LoggerFactory.getLogger(TransferDataService.class);

    private final TargetStorageService targetStorageService;

    TransferDataService(TargetStorageService targetStorageService) {
        this.targetStorageService = targetStorageService;
    }

    void cleanExistingTable(Table table) {
        targetStorageService.cleanExistingTable(table);
    }

    void transferData(DataIterable<TableRow> dataIterable, SyncTableData syncTableData) {
        final long total = dataIterable.countAll();
        int currentPage = 1;
        for (List<TableRow> values : dataIterable) {
            if (!values.isEmpty()) {
                final Stopwatch stopwatch = Stopwatch.createStarted();
                logger.info("Start transferring next batch of data (size={}) from table={}...", values.size(), syncTableData.getTable());
                targetStorageService.applyDmlChanges(syncTableData.getTable(), new DataChanges(values, syncTableData.getSyncDataType()));
                logger.info(
                        "{} of {} total rows from table={} were transferred, duration={}.",
                        syncTableData.getSyncBatchSize() * (currentPage - 1) + values.size(),
                        total, syncTableData.getTable(), stopwatch
                );
                currentPage++;
            }
        }
    }
}
