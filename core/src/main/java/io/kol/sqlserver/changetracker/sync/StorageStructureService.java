package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.model.Table;

import java.util.List;

public interface StorageStructureService {

    void initStructure(List<Table> tables, boolean dropAllExistingTables);

    void modifyStructure(List<Table> tables, boolean dropMissedTables);
}
