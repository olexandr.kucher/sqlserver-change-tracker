package io.kol.sqlserver.changetracker.exception;

public class PageDataReadException extends RuntimeException {
    public PageDataReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
