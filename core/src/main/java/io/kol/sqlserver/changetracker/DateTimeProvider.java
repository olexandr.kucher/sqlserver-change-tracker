package io.kol.sqlserver.changetracker;

import java.time.LocalDateTime;

@FunctionalInterface
public interface DateTimeProvider {
    LocalDateTime getNow();
}
