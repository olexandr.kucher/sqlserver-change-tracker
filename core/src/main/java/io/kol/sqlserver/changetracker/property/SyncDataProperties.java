package io.kol.sqlserver.changetracker.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@ConfigurationProperties(prefix = "sync.data")
public class SyncDataProperties {
    private int defaultBatchSize;
    private int parallelThreadsCount;
    private int maxSyncFailuresCount;
    private boolean useBufferedDataReader;
    private boolean dropMissedTablesOnSync;
    private boolean useParallelDataTransfer;
    private Map<String, Integer> batchSizes;

    public int getDefaultBatchSize() {
        return defaultBatchSize;
    }

    public void setDefaultBatchSize(int defaultBatchSize) {
        this.defaultBatchSize = defaultBatchSize;
    }

    public int getParallelThreadsCount() {
        return parallelThreadsCount;
    }

    public void setParallelThreadsCount(int parallelThreadsCount) {
        this.parallelThreadsCount = parallelThreadsCount;
    }

    public int getMaxSyncFailuresCount() {
        return maxSyncFailuresCount;
    }

    public void setMaxSyncFailuresCount(int maxSyncFailuresCount) {
        this.maxSyncFailuresCount = maxSyncFailuresCount;
    }

    public boolean isUseBufferedDataReader() {
        return useBufferedDataReader;
    }

    public void setUseBufferedDataReader(boolean useBufferedDataReader) {
        this.useBufferedDataReader = useBufferedDataReader;
    }

    public boolean isDropMissedTablesOnSync() {
        return dropMissedTablesOnSync;
    }

    public void setDropMissedTablesOnSync(boolean dropMissedTablesOnSync) {
        this.dropMissedTablesOnSync = dropMissedTablesOnSync;
    }

    public boolean isUseParallelDataTransfer() {
        return useParallelDataTransfer;
    }

    public void setUseParallelDataTransfer(boolean useParallelDataTransfer) {
        this.useParallelDataTransfer = useParallelDataTransfer;
    }

    public Map<String, Integer> getBatchSizes() {
        return batchSizes;
    }

    public void setBatchSizes(Map<String, Integer> batchSizes) {
        this.batchSizes = batchSizes;
    }
}
