package io.kol.sqlserver.changetracker.model;

import java.util.Objects;

public class TargetTable {
    private final String tableSchema;
    private final String tableName;

    public TargetTable(String tableSchema, String tableName) {
        this.tableSchema = tableSchema;
        this.tableName = tableName;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        TargetTable that = (TargetTable) obj;
        return Objects.equals(tableSchema, that.tableSchema) && Objects.equals(tableName, that.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableSchema, tableName);
    }
}
