package io.kol.sqlserver.changetracker.model;

public enum SyncDataType {
    FULL, DELTA
}
