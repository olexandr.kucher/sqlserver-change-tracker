package io.kol.sqlserver.changetracker.pagination;

import java.util.List;

public interface DataIterable<T> extends Iterable<List<T>> {
    long countAll();
}
