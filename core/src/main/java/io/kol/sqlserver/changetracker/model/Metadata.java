package io.kol.sqlserver.changetracker.model;

import java.time.LocalDateTime;

public class Metadata {
    private final MetadataType type;
    private final Object value;
    private final LocalDateTime timestamp;

    public Metadata(MetadataType type, Object value, LocalDateTime timestamp) {
        this.type = type;
        this.value = value;
        this.timestamp = timestamp;
    }

    public MetadataType getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
