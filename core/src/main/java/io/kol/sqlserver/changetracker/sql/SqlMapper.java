package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;

public interface SqlMapper {
    int VERSION_COLUMN_REMAP_TYPE_LENGTH = 8;
    String VERSION_COLUMN_REMAP_TYPE_NAME = "binary";
    String VERSION_COLUMN_TYPE_VERSION = "rowversion";
    String VERSION_COLUMN_TYPE_TIMESTAMP = "timestamp";

    String remapSqlName(String sqlName);

    Column remapColumn(Column column);
}
