package io.kol.sqlserver.changetracker.exception;

public class IncompatibleColumnChangeException extends RuntimeException {
    public IncompatibleColumnChangeException(Throwable cause) {
        super(cause);
    }
}
