package io.kol.sqlserver.changetracker.model;

import java.util.Objects;
import java.util.stream.Stream;

public enum ChangeOperation {
    CREATE("I"),
    UPDATE("U"),
    DELETE("D");

    private final String operationCode;

    ChangeOperation(String operationCode) {
        this.operationCode = operationCode;
    }

    public static ChangeOperation findByCode(String code) {
        return Stream.of(values())
                .filter(co -> Objects.equals(co.operationCode, code))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown change operation with code=" + code));
    }
}
