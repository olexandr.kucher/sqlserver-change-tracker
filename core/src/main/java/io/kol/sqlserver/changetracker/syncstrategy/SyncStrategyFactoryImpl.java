package io.kol.sqlserver.changetracker.syncstrategy;

import io.kol.sqlserver.changetracker.SettingsService;
import io.kol.sqlserver.changetracker.TrackedDatabaseService;
import io.kol.sqlserver.changetracker.model.SyncTableData;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.pagination.DataIterableFactory;
import io.kol.sqlserver.changetracker.property.SyncDataProperties;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

import static io.kol.sqlserver.changetracker.model.SyncDataType.DELTA;
import static io.kol.sqlserver.changetracker.model.SyncDataType.FULL;
import static java.util.Objects.requireNonNullElse;

@Component
class SyncStrategyFactoryImpl implements SyncStrategyFactory {
    private final int defaultBatchSize;
    private final SettingsService settingsService;
    private final Map<String, Integer> batchSizes;
    private final TransferDataService transferDataService;
    private final DataIterableFactory dataIterableFactory;
    private final TrackedDatabaseService trackedDatabaseService;

    SyncStrategyFactoryImpl(SettingsService settingsService,
                            SyncDataProperties syncDataProperties,
                            TransferDataService transferDataService,
                            DataIterableFactory dataIterableFactory,
                            TrackedDatabaseService trackedDatabaseService) {
        this.settingsService = settingsService;
        this.transferDataService = transferDataService;
        this.dataIterableFactory = dataIterableFactory;
        this.trackedDatabaseService = trackedDatabaseService;
        this.defaultBatchSize = syncDataProperties.getDefaultBatchSize();
        this.batchSizes = requireNonNullElse(syncDataProperties.getBatchSizes(), Collections.emptyMap());
    }

    @Override
    public SyncDataStrategy determineSyncStrategy(Table table) {
        final long lastSyncVersion = settingsService.getLastSyncVersion(table);
        final long currentVersion = trackedDatabaseService.getCurrentVersion();
        final long minimumValidVersion = trackedDatabaseService.getMinimumValidVersion(table);

        if (lastSyncVersion < minimumValidVersion || lastSyncVersion > currentVersion) {
            return new FullReInitStrategy(
                    SyncTableData.builder()
                            .setTable(table)
                            .setLastSyncVersion(lastSyncVersion)
                            .setCurrentVersion(currentVersion)
                            .setSyncBatchSize(getBatchSize(table))
                            .setSyncDataType(FULL)
                            .build(),
                    dataIterableFactory,
                    transferDataService,
                    trackedDatabaseService
            );
        }

        if (lastSyncVersion == currentVersion) {
            return new NoopStrategy(currentVersion);
        }

        return new SyncDeltaStrategy(
                SyncTableData.builder()
                        .setTable(table)
                        .setLastSyncVersion(lastSyncVersion)
                        .setCurrentVersion(currentVersion)
                        .setSyncBatchSize(getBatchSize(table))
                        .setSyncDataType(DELTA)
                        .build(),
                transferDataService,
                dataIterableFactory,
                trackedDatabaseService
        );
    }

    private int getBatchSize(Table table) {
        return batchSizes.getOrDefault(table.getTableName(), defaultBatchSize);
    }
}
