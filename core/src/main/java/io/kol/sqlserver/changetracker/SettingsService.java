package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.Metadata;
import io.kol.sqlserver.changetracker.model.MetadataType;
import io.kol.sqlserver.changetracker.model.Table;

import java.util.List;

public interface SettingsService {
    Metadata getMetadata(MetadataType type);

    void saveMetadata(MetadataType type, Object value);

    long getLastSyncVersion(Table table);

    List<Table> getAllTrackedTables();

    void saveLastSyncVersion(Table table, long lastSyncVersion);

    void saveTrackedTables(List<Table> tables);

    void markForFullReInit(Table table);

    void markForFullReInitIfRequired(Table table);

    void deleteLastSyncData();
}
