package io.kol.sqlserver.changetracker.sync;

public interface ChangeTrackingSyncService {
    void init(boolean dropAllExistingTables);

    void sync(boolean dropMissedTables);

    void sync();
}
