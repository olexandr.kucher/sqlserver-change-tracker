package io.kol.sqlserver.changetracker.sync;

import io.kol.sqlserver.changetracker.model.Column;

public interface ColumnEqualComparator {

    boolean isEquals(Column sourceColumn, Column targetColumn);
}