package io.kol.sqlserver.changetracker.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kol.sqlserver.changetracker.SettingsService;

import java.time.LocalDateTime;

public enum MetadataType {
    INIT_EPOCH_SECONDS {
        @Override
        public Object readValueFromJson(ObjectMapper objectMapper, String json) {
            try {
                return objectMapper.readValue(json, Long.class);
            } catch (JsonProcessingException e) {
                return 0L;
            }
        }

        @Override
        public Metadata emptyMetadata(LocalDateTime timestamp) {
            return new Metadata(this, 0L, timestamp);
        }
    },
    SYNC_EPOCH_SECONDS {
        @Override
        public Object readValueFromJson(ObjectMapper objectMapper, String json) {
            try {
                return objectMapper.readValue(json, Long.class);
            } catch (JsonProcessingException e) {
                return 0L;
            }
        }

        @Override
        public Metadata emptyMetadata(LocalDateTime timestamp) {
            return new Metadata(this, 0L, timestamp);
        }
    },
    TRACKED_TABLES_COUNT {
        @Override
        public Object readValueFromJson(ObjectMapper objectMapper, String json) {
            try {
                return objectMapper.readValue(json, Integer.class);
            } catch (JsonProcessingException e) {
                return 0L;
            }
        }

        @Override
        public Metadata emptyMetadata(LocalDateTime timestamp) {
            return new Metadata(this, 0, timestamp);
        }
    };

    public abstract Object readValueFromJson(ObjectMapper objectMapper, String json);

    public abstract Metadata emptyMetadata(LocalDateTime timestamp);

    @SuppressWarnings("unchecked")
    public <T> T metadata(SettingsService settingsService) {
        return (T) settingsService.getMetadata(this).getValue();
    }
}
