package io.kol.sqlserver.changetracker.pagination;

import io.kol.sqlserver.changetracker.model.PageRequest;

import java.util.List;

@FunctionalInterface
public interface PageSupplier<T> {
    List<T> getPage(PageRequest pageRequest);
}
