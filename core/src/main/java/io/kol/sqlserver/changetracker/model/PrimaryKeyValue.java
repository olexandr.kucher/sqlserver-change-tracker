package io.kol.sqlserver.changetracker.model;

public class PrimaryKeyValue {
    private final Object[] values;

    public PrimaryKeyValue(Object... values) {
        this.values = values;
    }

    public Object[] getValues() {
        return values;
    }
}
