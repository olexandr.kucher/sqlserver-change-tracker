package io.kol.sqlserver.changetracker.model;

public final class PageRequest {
    private final int pageNumber;
    private final int itemsPerPage;

    public PageRequest(int pageNumber, int itemsPerPage) {
        if (pageNumber <= 0) {
            throw new IllegalArgumentException("PageNumber cannot be less or equal to 0.");
        }
        if (itemsPerPage <= 0) {
            throw new IllegalArgumentException("ItemsPerPage cannot be less or equal to 0.");
        }
        this.pageNumber = pageNumber;
        this.itemsPerPage = itemsPerPage;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public int countOffset() {
        return (pageNumber - 1) * itemsPerPage;
    }

    public PageRequest next() {
        return new PageRequest(pageNumber + 1, itemsPerPage);
    }
}
