package io.kol.sqlserver.changetracker.model;

public final class SyncTableData {
    private final Table table;
    private final long lastSyncVersion;
    private final long currentVersion;
    private final int syncBatchSize;
    private final SyncDataType syncDataType;

    private SyncTableData(Builder builder) {
        this.table = builder.table;
        this.lastSyncVersion = builder.lastSyncVersion;
        this.currentVersion = builder.currentVersion;
        this.syncBatchSize = builder.syncBatchSize;
        this.syncDataType = builder.syncDataType;
    }

    public Table getTable() {
        return table;
    }

    public long getLastSyncVersion() {
        return lastSyncVersion;
    }

    public long getCurrentVersion() {
        return currentVersion;
    }

    public int getSyncBatchSize() {
        return syncBatchSize;
    }

    public SyncDataType getSyncDataType() {
        return syncDataType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Table table;
        private long lastSyncVersion;
        private long currentVersion;
        private int syncBatchSize;
        private SyncDataType syncDataType;

        private Builder() {
        }

        public Builder setTable(Table table) {
            this.table = table;
            return this;
        }

        public Builder setLastSyncVersion(long lastSyncVersion) {
            this.lastSyncVersion = lastSyncVersion;
            return this;
        }

        public Builder setCurrentVersion(long currentVersion) {
            this.currentVersion = currentVersion;
            return this;
        }

        public Builder setSyncBatchSize(int syncBatchSize) {
            this.syncBatchSize = syncBatchSize;
            return this;
        }

        public Builder setSyncDataType(SyncDataType syncDataType) {
            this.syncDataType = syncDataType;
            return this;
        }

        public SyncTableData build() {
            return new SyncTableData(this);
        }
    }
}
