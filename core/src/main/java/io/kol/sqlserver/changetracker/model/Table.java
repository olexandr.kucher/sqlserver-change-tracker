package io.kol.sqlserver.changetracker.model;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Table {
    private final String tableSchema;
    private final String tableName;
    private final PrimaryKey primaryKey;
    private final List<Column> columns;

    private Table(Builder builder) {
        this.tableSchema = builder.tableSchema;
        this.tableName = builder.tableName;
        this.primaryKey = builder.primaryKey;
        this.columns = builder.columns
                .stream()
                .sorted(Comparator.comparingInt(Column::getPosition))
                .collect(Collectors.toUnmodifiableList());
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String tableSchema;
        private String tableName;
        private PrimaryKey primaryKey;
        private List<Column> columns;

        private Builder() {
        }

        public Builder setTableSchema(String tableSchema) {
            this.tableSchema = tableSchema;
            return this;
        }

        public Builder setTableName(String tableName) {
            this.tableName = tableName;
            return this;
        }

        public Builder setPrimaryKey(PrimaryKey primaryKey) {
            this.primaryKey = primaryKey;
            return this;
        }

        public Builder setColumns(List<Column> columns) {
            this.columns = columns;
            return this;
        }

        public Builder of(Table table) {
            this.tableSchema = table.tableSchema;
            this.tableName = table.tableName;
            this.primaryKey = table.primaryKey;
            this.columns = table.columns;
            return this;
        }

        public Table build() {
            return new Table(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Table table = (Table) obj;
        return Objects.equals(tableSchema, table.tableSchema)
               && Objects.equals(tableName, table.tableName)
               && Objects.equals(primaryKey, table.primaryKey)
               && Objects.equals(columns, table.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableSchema, tableName, primaryKey, columns);
    }

    @Override
    public String toString() {
        return "[" + tableSchema + "].[" + tableName + "]";
    }
}
