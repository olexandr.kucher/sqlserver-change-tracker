package io.kol.sqlserver.changetracker.model;

import java.util.Map;

public final class TableRow {
    private final PrimaryKeyValue primaryKey;
    private final ChangeOperation operation;
    private final Map<String, Object> row;

    public TableRow(PrimaryKeyValue primaryKey, ChangeOperation operation, Map<String, Object> row) {
        this.primaryKey = primaryKey;
        this.operation = operation;
        this.row = row;
    }

    public PrimaryKeyValue getPrimaryKey() {
        return primaryKey;
    }

    public ChangeOperation getOperation() {
        return operation;
    }

    public Map<String, Object> getRow() {
        return row;
    }
}
