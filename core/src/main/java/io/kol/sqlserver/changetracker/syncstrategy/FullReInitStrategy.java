package io.kol.sqlserver.changetracker.syncstrategy;

import com.google.common.base.Stopwatch;
import io.kol.sqlserver.changetracker.TrackedDatabaseService;
import io.kol.sqlserver.changetracker.model.SyncTableData;
import io.kol.sqlserver.changetracker.model.TableRow;
import io.kol.sqlserver.changetracker.pagination.DataIterable;
import io.kol.sqlserver.changetracker.pagination.DataIterableFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.kol.sqlserver.changetracker.model.SyncDataType.FULL;

class FullReInitStrategy implements SyncDataStrategy {
    private final Logger logger = LoggerFactory.getLogger(FullReInitStrategy.class);

    private final SyncTableData syncTableData;
    private final DataIterableFactory dataIterableFactory;
    private final TransferDataService transferDataService;
    private final TrackedDatabaseService trackedDatabaseService;

    FullReInitStrategy(SyncTableData syncTableData,
                       DataIterableFactory dataIterableFactory,
                       TransferDataService transferDataService,
                       TrackedDatabaseService trackedDatabaseService) {
        this.syncTableData = syncTableData;
        this.dataIterableFactory = dataIterableFactory;
        this.transferDataService = transferDataService;
        this.trackedDatabaseService = trackedDatabaseService;
    }

    @Override
    public long sync() {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        logger.info(
                "Start full re-import of data for table={}, up to version={}...",
                syncTableData.getTable(), syncTableData.getCurrentVersion()
        );

        logger.info("Clear existing table={} in the target storage...", syncTableData.getTable());
        transferDataService.cleanExistingTable(syncTableData.getTable());
        logger.info("Table={} cleaned successfully in the target storage.", syncTableData.getTable());

        logger.info("Start import data for table={} with batch size={}...", syncTableData.getTable(), syncTableData.getSyncBatchSize());
        final DataIterable<TableRow> iterable = dataIterableFactory.createDataIterable(FULL, pageRequest -> trackedDatabaseService
                .getAllTableValues(
                        syncTableData.getTable(),
                        syncTableData.getCurrentVersion(),
                        pageRequest
                ), () -> trackedDatabaseService.countAllTableValues(
                        syncTableData.getTable(),
                        syncTableData.getCurrentVersion()
                ), syncTableData.getSyncBatchSize(),
                syncTableData.getTable()
        );

        transferDataService.transferData(iterable, syncTableData);

        logger.info(
                "Full re-import of data for table={} (up to version={}) completed successfully (spent time={}).",
                syncTableData.getTable(), syncTableData.getCurrentVersion(), stopwatch.stop()
        );
        return syncTableData.getCurrentVersion();
    }
}
