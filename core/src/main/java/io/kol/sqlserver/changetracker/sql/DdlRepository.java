package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;

import java.util.List;
import java.util.Map;

public interface DdlRepository {

    List<TargetTable> getAllTablesInSchema(String schemaName);

    Map<TargetTable, List<Column>> getAllTableColumns();

    boolean isSchemaExists(String schema);

    void createSchema(String schema);

    void dropSchema(String schema);

    void createTable(Table table);

    void dropTable(TargetTable table);

    void createColumn(Table table, Column column);

    void alterColumn(Table table, Column sourceColumn, Column targetColumn);

    void dropColumn(Table table, Column column);
}
