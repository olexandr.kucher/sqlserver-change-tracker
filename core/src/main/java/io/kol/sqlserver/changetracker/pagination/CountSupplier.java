package io.kol.sqlserver.changetracker.pagination;

@FunctionalInterface
public interface CountSupplier {
    long getCount();
}
