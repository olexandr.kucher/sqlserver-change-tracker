package io.kol.sqlserver.changetracker.syncstrategy;

@FunctionalInterface
public interface SyncDataStrategy {

    long sync();
}
