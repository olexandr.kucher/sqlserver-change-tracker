package io.kol.sqlserver.changetracker.sql;

import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.Table;
import io.kol.sqlserver.changetracker.model.TargetTable;

import java.util.List;

public interface SqlDefinitionCreator {
    String createSchema(String schema);

    String dropSchema(String schema);

    String createTable(Table table);

    String dropTable(TargetTable table);

    String insert(Table table);

    String merge(Table table);

    String update(Table table);

    String delete(Table table);

    String truncate(Table table);

    String createColumn(Table table, Column column);

    List<String> alterColumn(Table table, Column sourceColumn, Column targetColumn);

    String dropColumn(Table table, Column column);
}
