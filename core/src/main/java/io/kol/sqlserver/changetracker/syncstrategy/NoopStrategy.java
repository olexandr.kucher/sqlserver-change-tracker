package io.kol.sqlserver.changetracker.syncstrategy;

class NoopStrategy implements SyncDataStrategy {
    private final long currentVersion;

    NoopStrategy(long currentVersion) {
        this.currentVersion = currentVersion;
    }

    @Override
    public long sync() {
        return currentVersion;
    }
}
