package io.kol.sqlserver.changetracker.syncstrategy;

import io.kol.sqlserver.changetracker.model.Table;

public interface SyncStrategyFactory {
    SyncDataStrategy determineSyncStrategy(Table table);
}
