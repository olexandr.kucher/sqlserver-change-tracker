package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.annotation.SqlServerDatabase;
import io.kol.sqlserver.changetracker.model.DbColumn;
import io.kol.sqlserver.changetracker.model.DbPrimaryKey;
import io.kol.sqlserver.changetracker.model.DbTable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
class TableMetaDataRepository {
    private static final String IS_NULLABLE_COLUMN_MARKER = "YES";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    TableMetaDataRepository(@SqlServerDatabase NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    List<DbColumn> getChangeTrackingTableColumns() {
        final String sql = """
                    SELECT
                     s.name AS schemaName, OBJECT_NAME(t.object_id) AS tableName,
                     col.ORDINAL_POSITION, col.COLUMN_NAME, col.DATA_TYPE,
                     col.CHARACTER_MAXIMUM_LENGTH, col.IS_NULLABLE, col.COLLATION_NAME
                    FROM sys.change_tracking_tables c
                        JOIN sys.tables t ON t.object_id = c.object_id
                        JOIN sys.schemas s ON s.schema_id = t.schema_id
                        JOIN INFORMATION_SCHEMA.COLUMNS col ON col.TABLE_SCHEMA = s.name AND col.TABLE_NAME = OBJECT_NAME(t.object_id);
                """;
        return jdbcTemplate.query(sql, dbColumnRowMapper());
    }

    List<DbPrimaryKey> getChangeTrackingTablePrimaryKeys() {
        final String sql = """
                    SELECT
                     k.TABLE_SCHEMA AS schemaName, k.TABLE_NAME AS tableName,
                     col.ORDINAL_POSITION, col.COLUMN_NAME, col.DATA_TYPE,
                     col.CHARACTER_MAXIMUM_LENGTH, col.IS_NULLABLE, col.COLLATION_NAME
                    FROM sys.change_tracking_tables c
                        JOIN sys.tables t ON t.object_id = c.object_id
                        JOIN sys.schemas s ON s.schema_id = t.schema_id
                        JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE k ON k.TABLE_SCHEMA = s.name
                            AND k.TABLE_NAME = OBJECT_NAME(t.object_id)
                        JOIN INFORMATION_SCHEMA.COLUMNS col ON col.TABLE_SCHEMA = s.name
                            AND col.TABLE_NAME = OBJECT_NAME(t.object_id)
                            AND col.COLUMN_NAME = k.COLUMN_NAME
                    WHERE OBJECTPROPERTY(OBJECT_ID(k.CONSTRAINT_SCHEMA + '.' + QUOTENAME(k.CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
                """;
        final List<DbColumn> primaryKeyColumns = jdbcTemplate.query(sql, dbColumnRowMapper());
        return primaryKeyColumns.stream()
                .collect(Collectors.groupingBy(DbColumn::getTable))
                .entrySet()
                .stream()
                .map(entry -> new DbPrimaryKey(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private RowMapper<DbColumn> dbColumnRowMapper() {
        return (rs, i) -> DbColumn.builder()
                .setTable(new DbTable(
                        rs.getString("schemaName"),
                        rs.getString("tableName")
                ))
                .setPosition(rs.getInt("ORDINAL_POSITION"))
                .setColumnName(rs.getString("COLUMN_NAME"))
                .setDataType(rs.getString("DATA_TYPE"))
                .setMaxLength((Integer) rs.getObject("CHARACTER_MAXIMUM_LENGTH"))
                .setNullable(IS_NULLABLE_COLUMN_MARKER.equalsIgnoreCase(rs.getString("IS_NULLABLE")))
                .setCollationName(rs.getString("COLLATION_NAME"))
                .build();
    }
}
