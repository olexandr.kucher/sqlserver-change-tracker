package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
class TrackedDatabaseServiceImpl implements TrackedDatabaseService {
    private final TableDataRepository tableDataRepository;
    private final TableMetaDataRepository tableMetaDataRepository;
    private final ChangeTrackingMetaDataRepository changeTrackingMetaDataRepository;
    private final TableMetaDataConverter tableMetaDataConverter;

    TrackedDatabaseServiceImpl(TableDataRepository tableDataRepository,
                               TableMetaDataRepository tableMetaDataRepository,
                               ChangeTrackingMetaDataRepository changeTrackingMetaDataRepository,
                               TableMetaDataConverter tableMetaDataConverter) {
        this.tableDataRepository = tableDataRepository;
        this.tableMetaDataRepository = tableMetaDataRepository;
        this.changeTrackingMetaDataRepository = changeTrackingMetaDataRepository;
        this.tableMetaDataConverter = tableMetaDataConverter;
    }

    @Override
    public long getCurrentVersion() {
        return changeTrackingMetaDataRepository.getCurrentVersion();
    }

    @Override
    public long getMinimumValidVersion(Table table) {
        return changeTrackingMetaDataRepository.getMinimumValidVersion(table);
    }

    @Override
    public List<Table> getAllTrackedTables() {
        final Map<DbTable, List<DbColumn>> tableColumns = tableMetaDataRepository
                .getChangeTrackingTableColumns()
                .stream()
                .collect(Collectors.groupingBy(DbColumn::getTable));
        final Map<DbTable, List<DbColumn>> tablePrimaryKey = tableMetaDataRepository
                .getChangeTrackingTablePrimaryKeys()
                .stream()
                .collect(Collectors.toMap(DbPrimaryKey::getTable, DbPrimaryKey::getColumns));

        return tableColumns.entrySet()
                .stream()
                .map(entry -> tableMetaDataConverter.convertTableData(entry, tablePrimaryKey))
                .sorted(Comparator
                        .comparing(Table::getTableSchema)
                        .thenComparing(Table::getTableName)
                ).collect(Collectors.toList());
    }

    @Override
    public long countAllTableValues(Table table, long currentVersion) {
        return tableDataRepository.countAll(table, currentVersion);
    }

    @Override
    public long countAllUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion) {
        return tableDataRepository.countAllUntrackedTableValues(table, lastSyncVersion, currentVersion);
    }

    @Override
    public List<TableRow> getAllTableValues(Table table, long currentVersion, PageRequest pageRequest) {
        return tableDataRepository.getAllTableValues(table, currentVersion, pageRequest);
    }

    @Override
    public List<TableRow> getUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion, PageRequest pageRequest) {
        return tableDataRepository.getUntrackedTableValues(table, lastSyncVersion, currentVersion, pageRequest);
    }
}
