package io.kol.sqlserver.changetracker.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.kol.sqlserver.changetracker.annotation.SqlServerDatabase;
import io.kol.sqlserver.changetracker.property.DatabaseProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
class TrackedDatabaseConfig {
    @ConfigurationProperties("tracked.database")
    @SqlServerDatabase
    @Bean
    DatabaseProperties targetDatabaseProperties() {
        return new DatabaseProperties();
    }

    @SqlServerDatabase
    @Bean(destroyMethod = "close")
    HikariDataSource trackedDatabaseDataSource(@SqlServerDatabase DatabaseProperties properties) {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getJdbcUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        config.setDriverClassName(com.microsoft.sqlserver.jdbc.SQLServerDriver.class.getName());
        return new HikariDataSource(config);
    }

    @SqlServerDatabase
    @Bean
    NamedParameterJdbcTemplate trackedDatabaseJdbcTemplate(@SqlServerDatabase HikariDataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @SqlServerDatabase
    @Bean
    PlatformTransactionManager trackedDatabaseTransactionalManager(@SqlServerDatabase HikariDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
