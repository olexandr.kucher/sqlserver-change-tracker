package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.annotation.SqlServerDatabase;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
class ChangeTrackingMetaDataRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    ChangeTrackingMetaDataRepository(@SqlServerDatabase NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    long getCurrentVersion() {
        final String sql = "SELECT CHANGE_TRACKING_CURRENT_VERSION()";
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of(), long.class);
    }

    long getMinimumValidVersion(Table table) {
        final String sql = "SELECT CHANGE_TRACKING_MIN_VALID_VERSION(OBJECT_ID(:table_name))";
        final String tableName = String.format("[%s].[%s]", table.getTableSchema(), table.getTableName());
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of("table_name", tableName), long.class);
    }
}
