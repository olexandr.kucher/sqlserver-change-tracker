package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.model.DbColumn;
import io.kol.sqlserver.changetracker.model.DbTable;
import io.kol.sqlserver.changetracker.model.Column;
import io.kol.sqlserver.changetracker.model.PrimaryKey;
import io.kol.sqlserver.changetracker.model.Table;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Component
class TableMetaDataConverter {
    Table convertTableData(Entry<DbTable, List<DbColumn>> entry, Map<DbTable, List<DbColumn>> tablePrimaryKey) {
        return Table.builder()
                .setTableSchema(entry.getKey().getSchemaName())
                .setTableName(entry.getKey().getTableName())
                .setPrimaryKey(new PrimaryKey(convertColumns(tablePrimaryKey.get(entry.getKey()))))
                .setColumns(convertColumns(entry.getValue()))
                .build();
    }

    private List<Column> convertColumns(List<DbColumn> columns) {
        return columns.stream().map(this::convertColumn).collect(Collectors.toList());
    }

    private Column convertColumn(DbColumn column) {
        return Column.builder()
                .setPosition(column.getPosition())
                .setColumnName(column.getColumnName())
                .setDataType(column.getDataType())
                .setMaxLength(column.getMaxLength())
                .setNullable(column.isNullable())
                .setCollationName(column.getCollationName())
                .build();
    }
}
