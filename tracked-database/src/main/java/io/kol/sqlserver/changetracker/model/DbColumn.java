package io.kol.sqlserver.changetracker.model;

public class DbColumn {
    private final DbTable table;
    private final int position;
    private final String columnName;
    private final String dataType;
    private final Integer maxLength;
    private final boolean nullable;
    private final String collationName;

    private DbColumn(Builder builder) {
        this.table = builder.table;
        this.position = builder.position;
        this.columnName = builder.columnName;
        this.dataType = builder.dataType;
        this.maxLength = builder.maxLength;
        this.nullable = builder.nullable;
        this.collationName = builder.collationName;
    }

    public DbTable getTable() {
        return table;
    }

    public int getPosition() {
        return position;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public boolean isNullable() {
        return nullable;
    }

    public String getCollationName() {
        return collationName;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DbTable table;
        private int position;
        private String columnName;
        private String dataType;
        private Integer maxLength;
        private boolean nullable;
        private String collationName;

        private Builder() {
        }

        public Builder setTable(DbTable table) {
            this.table = table;
            return this;
        }

        public Builder setPosition(int position) {
            this.position = position;
            return this;
        }

        public Builder setColumnName(String columnName) {
            this.columnName = columnName;
            return this;
        }

        public Builder setDataType(String dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder setMaxLength(Integer maxLength) {
            this.maxLength = maxLength;
            return this;
        }

        public Builder setNullable(boolean nullable) {
            this.nullable = nullable;
            return this;
        }

        public Builder setCollationName(String collationName) {
            this.collationName = collationName;
            return this;
        }

        public Builder of(DbColumn dbColumn) {
            this.table = dbColumn.table;
            this.position = dbColumn.position;
            this.columnName = dbColumn.columnName;
            this.dataType = dbColumn.dataType;
            this.maxLength = dbColumn.maxLength;
            this.nullable = dbColumn.nullable;
            this.collationName = dbColumn.collationName;
            return this;
        }

        public DbColumn build() {
            return new DbColumn(this);
        }
    }
}