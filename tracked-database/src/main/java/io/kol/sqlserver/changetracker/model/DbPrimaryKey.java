package io.kol.sqlserver.changetracker.model;

import java.util.List;

public class DbPrimaryKey {
    private final DbTable table;
    private final List<DbColumn> columns;

    public DbPrimaryKey(DbTable table, List<DbColumn> columns) {
        this.table = table;
        this.columns = columns;
    }

    public DbTable getTable() {
        return table;
    }

    public List<DbColumn> getColumns() {
        return columns;
    }
}