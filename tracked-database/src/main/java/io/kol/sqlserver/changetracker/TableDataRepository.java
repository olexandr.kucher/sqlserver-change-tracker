package io.kol.sqlserver.changetracker;

import io.kol.sqlserver.changetracker.annotation.SqlServerDatabase;
import io.kol.sqlserver.changetracker.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.kol.sqlserver.changetracker.model.ChangeOperation.CREATE;
import static io.kol.sqlserver.changetracker.sql.SqlMapper.VERSION_COLUMN_TYPE_TIMESTAMP;
import static io.kol.sqlserver.changetracker.sql.SqlMapper.VERSION_COLUMN_TYPE_VERSION;

@Repository
class TableDataRepository {
    private static final String CHANGE_TRACKING_CHANGE_OPERATION = "changeOperation";
    private static final String CHANGE_TRACKING_CHANGE_ROW_ID_PREFIX = "changeRowId";

    private final Logger logger = LoggerFactory.getLogger(TableDataRepository.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;

    TableDataRepository(@SqlServerDatabase NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    long countAll(Table table, long currentVersion) {
        final String sql = String.format(
                """
                    SELECT count(*)
                    FROM [%s].[%s] t WITH (NOLOCK)
                        LEFT JOIN CHANGETABLE(CHANGES [%s].[%s], 0) ct ON %s
                    WHERE ct.SYS_CHANGE_VERSION IS NULL OR ct.SYS_CHANGE_VERSION <= :current_version
                """,
                table.getTableSchema(), table.getTableName(),
                table.getTableSchema(), table.getTableName(),
                primaryKeyColumnsJoin(table.getPrimaryKey())
        );

        logger.debug("Execute count: {}", sql);
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of("current_version", currentVersion), long.class);
    }

    long countAllUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion) {
        final String sql = String.format(
                """
                    SELECT count(*)
                    FROM [%s].[%s] t WITH (NOLOCK)
                        RIGHT OUTER JOIN CHANGETABLE(CHANGES [%s].[%s], :last_sync_version) ct ON %s
                    WHERE ct.SYS_CHANGE_VERSION > :last_sync_version AND ct.SYS_CHANGE_VERSION <= :current_version
                """,
                table.getTableSchema(), table.getTableName(),
                table.getTableSchema(), table.getTableName(),
                primaryKeyColumnsJoin(table.getPrimaryKey())
        );

        logger.debug("Execute select: {}", sql);
        //noinspection ConstantConditions
        return jdbcTemplate.queryForObject(sql, Map.of(
                "last_sync_version", lastSyncVersion,
                "current_version", currentVersion
        ), long.class);
    }

    List<TableRow> getAllTableValues(Table table, long currentVersion, PageRequest pageRequest) {
        final String sql = String.format(
                """
                    SELECT %s, t.*
                    FROM [%s].[%s] t WITH (NOLOCK)
                        LEFT JOIN CHANGETABLE(CHANGES [%s].[%s], 0) ct ON %s
                    WHERE ct.SYS_CHANGE_VERSION IS NULL OR ct.SYS_CHANGE_VERSION <= :current_version
                    ORDER BY %s
                    OFFSET :items_offset ROWS FETCH NEXT :items_per_page ROWS ONLY
                """,
                primaryKeyColumnsSelect(table.getPrimaryKey(), "t"),
                table.getTableSchema(), table.getTableName(),
                table.getTableSchema(), table.getTableName(),
                primaryKeyColumnsJoin(table.getPrimaryKey()),
                primaryKeyColumnsOrdering(table.getPrimaryKey(), "t")
        );

        logger.debug("Execute select: {}", sql);
        return jdbcTemplate.query(sql, Map.of(
                "current_version", currentVersion,
                "items_offset", pageRequest.countOffset(),
                "items_per_page", pageRequest.getItemsPerPage()
        ), (rs, i) -> new TableRow(
                getPrimaryKeyValue(rs, table.getPrimaryKey()),
                CREATE, getRowAsMap(rs)
        ));
    }

    List<TableRow> getUntrackedTableValues(Table table, long lastSyncVersion, long currentVersion, PageRequest pageRequest) {
        final String sql = String.format(
                """
                    SELECT ct.SYS_CHANGE_OPERATION AS changeOperation, %s, t.*
                    FROM [%s].[%s] t WITH (NOLOCK)
                        RIGHT OUTER JOIN CHANGETABLE(CHANGES [%s].[%s], :last_sync_version) ct ON %s
                    WHERE ct.SYS_CHANGE_VERSION > :last_sync_version AND ct.SYS_CHANGE_VERSION <= :current_version
                    ORDER BY %s
                    OFFSET :items_offset ROWS FETCH NEXT :items_per_page ROWS ONLY
                """,
                primaryKeyColumnsSelect(table.getPrimaryKey(), "ct"),
                table.getTableSchema(), table.getTableName(),
                table.getTableSchema(), table.getTableName(),
                primaryKeyColumnsJoin(table.getPrimaryKey()),
                primaryKeyColumnsOrdering(table.getPrimaryKey(), "ct")
        );

        logger.debug("Execute select: {}", sql);
        return jdbcTemplate.query(sql, Map.of(
                "last_sync_version", lastSyncVersion,
                "current_version", currentVersion,
                "items_offset", pageRequest.countOffset(),
                "items_per_page", pageRequest.getItemsPerPage()
        ), (rs, i) -> new TableRow(
                getPrimaryKeyValue(rs, table.getPrimaryKey()),
                ChangeOperation.findByCode(rs.getString("changeOperation")),
                getRowAsMap(rs)
        ));
    }

    private String primaryKeyColumnsSelect(PrimaryKey primaryKey, String alias) {
        return primaryKey.getColumns().stream()
                .map(col -> String.format("%s.[%s] AS changeRowId_%d", alias, col.getColumnName(), col.getPosition()))
                .collect(Collectors.joining(", "));
    }

    private String primaryKeyColumnsJoin(PrimaryKey primaryKey) {
        return primaryKey.getColumns().stream()
                .map(col -> String.format("t.[%s] = ct.[%s]", col.getColumnName(), col.getColumnName()))
                .collect(Collectors.joining(" AND "));
    }

    private String primaryKeyColumnsOrdering(PrimaryKey primaryKey, String alias) {
        return primaryKey.getColumns().stream()
                .map(col -> String.format("%s.[%s]", alias, col.getColumnName()))
                .collect(Collectors.joining(", "));
    }

    private PrimaryKeyValue getPrimaryKeyValue(ResultSet resultSet, PrimaryKey primaryKey) throws SQLException {
        final List<Object> values = new ArrayList<>();
        for (Column key : primaryKey.getColumns()) {
            values.add(resultSet.getObject(String.format("changeRowId_%d", key.getPosition())));
        }
        return new PrimaryKeyValue(values.toArray());
    }

    private Map<String, Object> getRowAsMap(ResultSet resultSet) throws SQLException {
        final Map<String, Object> map = new HashMap<>();
        final ResultSetMetaData metaData = resultSet.getMetaData();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            if (!isServiceColumn(metaData.getColumnLabel(i))) {
                final String columnName = metaData.getColumnName(i);
                final String columnTypeName = metaData.getColumnTypeName(i);
                if (isVersionColumn(columnTypeName)) {
                    map.put(columnName, resultSet.getString(columnName));
                } else {
                    map.put(columnName, resultSet.getObject(columnName));
                }
            }
        }
        return map;
    }

    private boolean isServiceColumn(String columnLabel) {
        return CHANGE_TRACKING_CHANGE_OPERATION.equals(columnLabel)
               || columnLabel.startsWith(CHANGE_TRACKING_CHANGE_ROW_ID_PREFIX);
    }

    private boolean isVersionColumn(String columnTypeName) {
        return VERSION_COLUMN_TYPE_TIMESTAMP.equalsIgnoreCase(columnTypeName)
                || VERSION_COLUMN_TYPE_VERSION.equalsIgnoreCase(columnTypeName);
    }
}
