package io.kol.sqlserver.changetracker.model;

import java.util.Objects;

public class DbTable {
    private final String schemaName;
    private final String tableName;

    public DbTable(String schemaName, String tableName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DbTable dbTable = (DbTable) obj;
        return Objects.equals(schemaName, dbTable.schemaName) && Objects.equals(tableName, dbTable.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schemaName, tableName);
    }
}